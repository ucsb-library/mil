<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['holding_id'])) && ($_GET['holding_id'] != "")) {
  $deleteSQL = sprintf("DELETE FROM ap_flights WHERE holding_id=%s",
                       GetSQLValueString($_GET['holding_id'], "int"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($deleteSQL, $MilWebAppsdb1mysql) or die(mysql_error());

//* delete ap_flights_loc_country record associated with that flight
  $deleteSQL2 = sprintf("DELETE FROM ap_flights_loc_country WHERE holding_id=%s",
                       GetSQLValueString($_GET['holding_id'], "int"));

  $Result2 = mysql_query($deleteSQL2, $MilWebAppsdb1mysql) or die(mysql_error());

//* delete ap_flights_loc_state record associated with that flight
  $deleteSQL3 = sprintf("DELETE FROM ap_flights_loc_state WHERE holding_id=%s",
                       GetSQLValueString($_GET['holding_id'], "int"));

  $Result3 = mysql_query($deleteSQL3, $MilWebAppsdb1mysql) or die(mysql_error());

//* delete ap_flights_loc_county record associated with that flight
  $deleteSQL4 = sprintf("DELETE FROM ap_flights_loc_county WHERE holding_id=%s",
                       GetSQLValueString($_GET['holding_id'], "int"));

  $Result4 = mysql_query($deleteSQL4, $MilWebAppsdb1mysql) or die(mysql_error());

  $deleteGoTo = "list_new.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['holding_id'])) {
  $colname_Recordset1 = $_GET['holding_id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM ap_flights WHERE holding_id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_free_result($Recordset1);
?>
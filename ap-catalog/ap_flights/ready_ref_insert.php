<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}
echo $_SERVER['QUERY_STRING'];

// format the image_name based upon the County that the user selected ... 02242011mer

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

	$whichCounty = $_POST[County];
	$whichImage = "";

	if ($whichCounty == "KERN") { $whichImage = "kern.jpg"; }
	if ($whichCounty == "LOS ANGELES") { $whichImage = "los_angeles.jpg"; }
	//<!-- if ($whichCounty == "ORANGE") { $whichImage = "orange.jpg"; } -->
	if ($whichCounty == "RIVERSIDE") { $whichImage = "riverside.jpg"; }
	if ($whichCounty == "SAN BERNARDINO") { $whichImage = "san_bernardino.jpg"; }
	if ($whichCounty == "SAN DIEGO") { $whichImage = "san_diego.jpg"; }
	if ($whichCounty == "SAN LUIS OBISPO") { $whichImage = "san_luis_obispo.jpg"; }
	if ($whichCounty == "SANTA BARBARA") { $whichImage = "santa_barbara.jpg"; }
	if ($whichCounty == "VENTURA") { $whichImage ="ventura.jpg"; }

	//echo "i am here";
	//echo "and the county is $whichCounty";

// format the region user input ... needs to be in uppercase (don't ask me why) ... 02252011mer

	$whichRegion = $_POST[Region];
    $whichRegion = strtoupper($whichRegion);

  $insertSQL = sprintf("INSERT INTO Ready_Ref (holding_id, County, Region, Format, Notes, Restrictions, image_name) VALUES (%s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['holding_id'], "int"),
                       GetSQLValueString($_POST['County'], "text"),
                       GetSQLValueString($whichRegion, "text"),
 //                      GetSQLValueString($_POST['Region'], "text"),
                       GetSQLValueString($_POST['Format'], "text"),
                       GetSQLValueString($_POST['Notes'], "text"),
                       GetSQLValueString($_POST['Restrictions'], "text"),
                       GetSQLValueString($whichImage, "text"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($insertSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $insertGoTo = "ready_ref_master.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert Frequently Requested Flight Record</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />
<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILfont-x-large">Insert <span class="MILfont-bold">Draft</span> Frequently Requested Catalog Record <br /><br />
        </div></td>
  </tr>
</table>


<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">

  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="MILwhite MILcenter" >
  <col width="220" />
  <col width="320" />
    <tr><td>&nbsp;</td></tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">Holding_id:</td>
      <td valign="top"><input name="holding_id" type="text" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
          <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">County:</td>
          <td >
            <select name="County">
            <option value="">Please select a County:</option>
            <option value="ALAMEDA"> Alameda </option>
            <option value="CONTRA COSTA"> Contra Costa </option>
            <option value="KERN"> Kern </option>
            <option value="LOS ANGELES"> Los Angeles </option>
            <option value="MARIN"> Marin </option>
            <option value="ORANGE"> Orange </option>
            <option value="RIVERSIDE"> Riverside </option>
            <option value="SACRAMENTO"> Sacramento </option>
            <option value="SAN BERNARDINO"> San Bernardino </option>
            <option value="SAN DIEGO"> San Diego </option>
            <option value="SAN FRANCISCO"> San Francisco </option>
            <option value="SAN JOAQUIN"> San Joaquin </option>
            <option value="SAN MATEO"> San Mateo </option>
            <option value="SAN LUIS OBISPO"> San Luis Obispo </option>
            <option value="SANTA BARBARA"> Santa Barbara </option>
            <option value="SANTA CLARA"> Santa Clara </option>
            <option value="SOLANO"> Solano </option>
            <option value="SONOMA"> Sonoma </option>
            <option value="VENTURA"> Ventura </option>
            </select>
         </td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">Region:</td>
      <td valign="top"><input name="Region" type="text" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">Prints held:</td>
	  <td><input type="radio" name="Format" value="P" /><span class="MILfont-edit"> Yes </span><br />
	      <input type="radio" name="Format" value="" checked/><span class="MILfont-edit"> No </span><br />
      </td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">Notes:</td>
      <td valign="top"><input name="Notes" type="text" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">Restrictions:</td>
      <td valign="top"><input name="Restrictions" type="text" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td align="right" valign="middle" nowrap="nowrap">&nbsp;</td>
      <td valign="top"><input type="submit" value="Insert record" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />

</form>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->

<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>

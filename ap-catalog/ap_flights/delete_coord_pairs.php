<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE coord_pairs SET deleted=%s WHERE id=%s",
                       GetSQLValueString($_POST['deleted'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($updateSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $updateGoTo = "list_coord_pairs.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM coord_pairs WHERE id = %s", GetSQLValueString($colname_Recordset1, "-1"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE HTML>
<head>
<title>MIL AP Flights - Delete Coord Pairs</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

</head>

<body>

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="62%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center" class="MILfont-large MILwarning-red">Are you sure you want to delete the following       Coord Pairs ?</div></td>
  </tr>
</table>
<form method="POST" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td colspan="2" align="right" nowrap><div align="center">
        <input name="submit" type="submit" value="Yes delete!">
        <input name="deleted" type="hidden" id="deleted2" value="yes">
      </div></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Holding_id:</span></td>
      <td><?php echo $row_Recordset1['holding_id']; ?> </td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">
        <input name="id" type="hidden" id="id2" value="<?php echo $row_Recordset1['id']; ?>">
      Filed_by:</span></td>
      <td><?php echo $row_Recordset1['filed_by']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Polygon_number:</span></td>
      <td><?php echo $row_Recordset1['polygon_number']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_dir:</span></td>
      <td><?php echo $row_Recordset1['coord1_lat_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_deg:</span></td>
      <td><?php echo $row_Recordset1['coord1_lat_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_min:</span></td>
      <td><?php echo $row_Recordset1['coord1_lat_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_dir:</span></td>
      <td><?php echo $row_Recordset1['coord1_long_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_deg:</span></td>
      <td><?php echo $row_Recordset1['coord1_long_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_min:</span></td>
      <td><?php echo $row_Recordset1['coord1_long_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_dir:</span></td>
      <td><?php echo $row_Recordset1['coord2_lat_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_deg:</span></td>
      <td><?php echo $row_Recordset1['coord2_lat_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_min:</span></td>
      <td><?php echo $row_Recordset1['coord2_lat_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_dir:</span></td>
      <td><?php echo $row_Recordset1['coord2_long_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_deg:</span></td>
      <td><?php echo $row_Recordset1['coord2_long_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_min:</span></td>
      <td><?php echo $row_Recordset1['coord2_long_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_dir:</span></td>
      <td><?php echo $row_Recordset1['coord3_lat_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_deg:</span></td>
      <td><?php echo $row_Recordset1['coord3_lat_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_min:</span></td>
      <td><?php echo $row_Recordset1['coord3_lat_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_dir:</span></td>
      <td><?php echo $row_Recordset1['coord3_long_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_deg:</span></td>
      <td><?php echo $row_Recordset1['coord3_long_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_min:</span></td>
      <td><?php echo $row_Recordset1['coord3_long_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_dir:</span></td>
      <td><?php echo $row_Recordset1['coord4_lat_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_deg:</span></td>
      <td><?php echo $row_Recordset1['coord4_lat_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_min:</span></td>
      <td><?php echo $row_Recordset1['coord4_lat_min']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_dir:</span></td>
      <td><?php echo $row_Recordset1['coord4_long_dir']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_deg:</span></td>
      <td><?php echo $row_Recordset1['coord4_long_deg']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_min:</span></td>
      <td><?php echo $row_Recordset1['coord4_long_min']; ?></td>
    </tr>
  </table>

  <input type="hidden" name="MM_update" value="form1">
</form>
<p align="center"><a href="list_coord_pairs.php">Return to Coordinate List without deleting</a></p>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>

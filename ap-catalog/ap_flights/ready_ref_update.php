<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

	$whichCounty = $_POST[County];
	$whichImage = "";

	if ($whichCounty == "KERN") { $whichImage = "kern.jpg"; }
	if ($whichCounty == "LOS ANGELES") { $whichImage = "los_angeles.jpg"; }
	//<!-- if ($whichCounty == "ORANGE") { $whichImage = "orange.jpg"; } -->
	if ($whichCounty == "RIVERSIDE") { $whichImage = "riverside.jpg"; }
	if ($whichCounty == "SAN BERNARDINO") { $whichImage = "san_bernardino.jpg"; }
	if ($whichCounty == "SAN DIEGO") { $whichImage = "san_diego.jpg"; }
	if ($whichCounty == "SAN LUIS OBISPO") { $whichImage = "san_luis_obispo.jpg"; }
	if ($whichCounty == "SANTA BARBARA") { $whichImage = "santa_barbara.jpg"; }
	if ($whichCounty == "VENTURA") { $whichImage ="ventura.jpg"; }

	//echo "i am here";
	//echo "and the county is $whichCounty";

// format the region user input ... needs to be in uppercase (don't ask me why) ... 02252011mer

	$whichRegion = $_POST[Region];
    $whichRegion = strtoupper($whichRegion);

     $updateSQL = sprintf("UPDATE Ready_Ref SET holding_id=%s, County=%s, Region=%s, Format=%s, Notes=%s, Restrictions=%s, image_name=%s WHERE id=%s",
                      GetSQLValueString($_POST['holding_id'], "int"),
                      GetSQLValueString($_POST['County'], "text"),
                      GetSQLValueString($whichRegion, "text"),
//*******8                      GetSQLValueString($_POST['Region'], "text"),
                      GetSQLValueString($_POST['Format'], "text"),
                      GetSQLValueString($_POST['Notes'], "text"),
                      GetSQLValueString($_POST['Restrictions'], "text"),
                      GetSQLValueString($whichImage, "text"),
//********                       GetSQLValueString($_POST['image_name'], "text"),
                      GetSQLValueString($_POST['id'], "int"));

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$Result1 = mysql_query($updateSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $updateGoTo = "ready_ref_master.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

$query_Recordset1 = sprintf("SELECT Ready_Ref.*, ap_flights.holding_id, ap_flights.filed_by FROM Ready_Ref, ap_flights WHERE Ready_Ref.id = %s AND Ready_Ref.holding_id = ap_flights.holding_id", GetSQLValueString($colname_Recordset1, "int"));

$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit <?php echo $row_Recordset1['filed_by']; ?> Frequently Requested Flight Record</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
     <td><div align="center" class="MILfont-x-large"> Edit Frequently Requested Catalog Record <br /><div>
     <div class="MILfont-large MILbottom-padding-10 MILline-height-150">  Filed by: <?php echo $row_Recordset1['filed_by']; ?></div>
  </tr>
</table>
<br />

<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">

  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="MILwhite MILcenter" >
  <col width="220" />
  <col width="320" />

    <tr><td>&nbsp;</td></tr>
    <tr valign="baseline">
      <td align="right" nowrap="nowrap" class="MILfont-edit">Holding id:</td>
      <td><input name="holding_id" type="text"  class="MILfont-input-box" value="<?php echo htmlentities($row_Recordset1['holding_id'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>

    <!-- *************************** -->
    <tr valign="baseline">
          <td align="right" valign="middle" nowrap="nowrap" class="MILfont-edit">County:</td>
          <td >
            <select class="MILfont-input-box" name="County">
            <option value="<?php echo htmlentities($row_Recordset1['County'], ENT_COMPAT, 'utf-8'); ?>" selected><?php echo htmlentities($row_Recordset1['County'], ENT_COMPAT, 'utf-8'); ?></option>
            <option value="ALAMEDA"> Alameda </option>
            <option value="CONTRA COSTA"> Contra Costa </option>
            <option value="KERN"> Kern </option>
            <option value="LOS ANGELES"> Los Angeles </option>
            <option value="MARIN"> Marin </option>
            <option value="ORANGE"> Orange </option>
            <option value="RIVERSIDE"> Riverside </option>
            <option value="SACRAMENTO"> Sacramento </option>
            <option value="SAN BERNARDINO"> San Bernardino </option>
            <option value="SAN DIEGO"> San Diego </option>
            <option value="SAN FRANCISCO"> San Francisco </option>
            <option value="SAN JOAQUIN"> San Joaquin </option>
            <option value="SAN MATEO"> San Mateo </option>
            <option value="SAN LUIS OBISPO"> San Luis Obispo </option>
            <option value="SANTA BARBARA"> Santa Barbara </option>
            <option value="SANTA CLARA"> Santa Clara </option>
            <option value="SOLANO"> Solano </option>
            <option value="SONOMA"> Sonoma </option>
            <option value="VENTURA"> Ventura </option>
            </select>
         </td>
    </tr>



    <!-- ***************************

    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><span class="MILfont-edit">County:</span></td>
      <td><input name="County" type="text" value="<?php echo htmlentities($row_Recordset1['County'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
      <td valign="top"><span class="MILfont-x-small">Current counties:  Kern, Los Angeles, Orange, Riverside, San Bernardino, San Diego, San Luis Obispo, Santa Barbara, Ventura</span></td>
    </tr>

    -->


    <tr valign="baseline">
      <td nowrap="nowrap" align="right" class="MILfont-edit">Region:</td>
      <td><input name="Region" type="text" class="MILfont-input-box" value="<?php echo htmlentities($row_Recordset1['Region'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>


    <tr valign="baseline">
      <td align="right" valign="top" nowrap="nowrap" class="MILfont-edit">Prints held:</td>

    <?php
       $format_choice = $row_Recordset1['Format'];
       if ($format_choice == "P") {
     ?>
    	  <td><input type="radio" class="MILfont-edit" name="Format" value="P" checked/> Yes<br />
	          <input type="radio" class="MILfont-edit" name="Format" value="" /> No<br />
           </td>

    <?php
      }
       else
      {
    ?>
	      <td><input type="radio" class="MILfont-edit" name="Format" value="P" /> Yes<br />
	          <input type="radio" class="MILfont-edit" name="Format" value="" checked/> No<br />
          </td>

 	<?php
      }
    ?>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" class="MILfont-edit">Notes:</td>
      <td><input name="Notes" type="text" class="MILfont-input-box" value="<?php echo htmlentities($row_Recordset1['Notes'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" class="MILfont-edit">Restrictions:</td>
      <td><input name="Restrictions" type="text" class="MILfont-input-box" value="<?php echo htmlentities($row_Recordset1['Restrictions'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>


    <!-- **************************
    <tr valign="baseline">
      <td nowrap="nowrap" align="right"><span class="MILfont-edit">Image_name:</span></td>
      <td><input name="image_name" type="text" value="<?php echo htmlentities($row_Recordset1['image_name'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
      <td valign="top"><span class="MILfont-x-small">Current images:  kern.jpg, los_angeles.jpg, orange.jpg, riverside.jpg, san_bernardino.jpg,  san_diego.jpg, san_luis_obispo.jpg, santa_barbara.jpg, ventura.jpg</span></td>
    </tr>
    ***************************-->

     <tr valign="baseline">
      <td align="right" nowrap="nowrap" class="MILfont-edit">Record id:</td>
      <td class="MILfont-edit"><?php echo $row_Recordset1['id']; ?> </td>
    </tr>

    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" class="MILfont-list" value="Update record" /></td>
    </tr>
  </table>
  <input type="hidden" name="id" value="<?php echo $row_Recordset1['id']; ?>" />
  <input type="hidden" name="MM_update" value="form1" />
</form>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->

<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>

<?php
$colname_Recordset1 = "1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = (get_magic_quotes_gpc()) ? $_GET['id'] : addslashes($_GET['id']);

}
?>

<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert Coord Pairs - AP Flights Cataloging</title>

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function YY_checkform() { //v4.71
//copyright (c)1998,2002 Yaromat.com
  var a=YY_checkform.arguments,oo=true,v='',s='',err=false,r,o,at,o1,t,i,j,ma,rx,cd,cm,cy,dte,at;
  for (i=1; i<a.length;i=i+4){
    if (a[i+1].charAt(0)=='#'){r=true; a[i+1]=a[i+1].substring(1);}else{r=false}
    o=MM_findObj(a[i].replace(/\[\d+\]/ig,""));
    o1=MM_findObj(a[i+1].replace(/\[\d+\]/ig,""));
    v=o.value;t=a[i+2];
    if (o.type=='text'||o.type=='password'||o.type=='hidden'){
      if (r&&v.length==0){err=true}
      if (v.length>0)
      if (t==1){ //fromto
        ma=a[i+1].split('_');if(isNaN(v)||v<ma[0]/1||v > ma[1]/1){err=true}
      } else if (t==2){
        rx=new RegExp("^[\\w\.=-]+@[\\w\\.-]+\\.[a-zA-Z]{2,4}$");if(!rx.test(v))err=true;
      } else if (t==3){ // date
        ma=a[i+1].split("#");at=v.match(ma[0]);
        if(at){
          cd=(at[ma[1]])?at[ma[1]]:1;cm=at[ma[2]]-1;cy=at[ma[3]];
          dte=new Date(cy,cm,cd);
          if(dte.getFullYear()!=cy||dte.getDate()!=cd||dte.getMonth()!=cm){err=true};
        }else{err=true}
      } else if (t==4){ // time
        ma=a[i+1].split("#");at=v.match(ma[0]);if(!at){err=true}
      } else if (t==5){ // check this 2
            if(o1.length)o1=o1[a[i+1].replace(/(.*\[)|(\].*)/ig,"")];
            if(!o1.checked){err=true}
      } else if (t==6){ // the same
            if(v!=MM_findObj(a[i+1]).value){err=true}
      }
    } else
    if (!o.type&&o.length>0&&o[0].type=='radio'){
          at = a[i].match(/(.*)\[(\d+)\].*/i);
          o2=(o.length>1)?o[at[2]]:o;
      if (t==1&&o2&&o2.checked&&o1&&o1.value.length/1==0){err=true}
      if (t==2){
        oo=false;
        for(j=0;j<o.length;j++){oo=oo||o[j].checked}
        if(!oo){s+='* '+a[i+3]+'\n'}
      }
    } else if (o.type=='checkbox'){
      if((t==1&&o.checked==false)||(t==2&&o.checked&&o1&&o1.value.length/1==0)){err=true}
    } else if (o.type=='select-one'||o.type=='select-multiple'){
      if(t==1&&o.selectedIndex/1==0){err=true}
    }else if (o.type=='textarea'){
      if(v.length<a[i+1]){err=true}
    }
    if (err){s+='* '+a[i+3]+'\n'; err=false}
  }
  if (s!=''){alert('The required information is incomplete or contains errors:\t\t\t\t\t\n\n'+s)}
  document.MM_returnValue = (s=='');
}
//-->
</script>

</head>

<body class="MILlight-grey">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="63%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILwhite MILfont-large">Insert New  Coord Pairs for
      MIL Air Photo Flights</div></td>
  </tr>
</table>
<form action="insert_coords_mysql.php" method="POST" name="form1">
  <table align="center">
        <tr valign="baseline">
          <td nowrap align="right"><div align="right"></div></td>
          <td>
            <div align="right">Holding_id:            </div></td>
          <td><div align="left">
            <input type="text" name="holding_id" size="25">
          </div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="right"></div></td>
          <td>
            <div align="right">Filed_by:            </div></td>
          <td><div align="left">
            <input type="text" name="filed_by" size="25">
          </div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="right"></div></td>
          <td>
            <div align="right">Polygon_number:            </div></td>
          <td><div align="left">
            <input type="text" name="polygon_number" size="5">
          </div></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
    </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center">
            <p><br>
              <br>
            </p>
          </div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="center" class="MILfont-small">Coord1_lat_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord1_lat_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord1_lat_min</div></td>
          <td><div align="center" class="MILfont-small">Coord1_long_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord1_long_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord1_long_min</div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">
            <div align="center" class="MILfont-small">
              <input type="text" name="coord1_lat_dir" value="N" size="5">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord1_lat_deg" value="" size="25">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord1_lat_min" value="" size="25">
            </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord1_long_dir" value="W" size="5">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord1_long_deg" value="" size="25">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord1_long_min" value="" size="25">
          </div></td>
        </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center" class="MILfont-small"><br>
          </div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="center" class="MILfont-small">Coord2_lat_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord2_lat_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord2_lat_min</div></td>
          <td align="right" nowrap><div align="center" class="MILfont-small">Coord2_long_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord2_long_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord2_long_min</div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">
            <div align="center" class="MILfont-small">
              <input type="text" name="coord2_lat_dir" value="N" size="5">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord2_lat_deg" value="" size="25">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord2_lat_min" value="" size="25">
            </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord2_long_dir" value="W" size="5">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord2_long_deg" value="" size="25">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord2_long_min" value="" size="25">
          </div></td>
        </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center" class="MILfont-small"><br>
          </div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="center" class="MILfont-small">Coord3_lat_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord3_lat_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord3_lat_min</div></td>
          <td align="right" nowrap><div align="center" class="MILfont-small">Coord3_long_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord3_long_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord3_long_min</div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">
            <div align="center" class="MILfont-small">
              <input type="text" name="coord3_lat_dir" value="N" size="5">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord3_lat_deg" value="" size="25">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord3_lat_min" value="" size="25">
            </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord3_long_dir" value="W" size="5">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord3_long_deg" value="" size="25">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord3_long_min" value="" size="25">
          </div></td>
        </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center" class="MILfont-small"><br>
          </div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right"><div align="center" class="MILfont-small">Coord4_lat_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord4_lat_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord4_lat_min</div></td>
          <td align="right" nowrap><div align="center" class="MILfont-small">Coord4_long_dir</div></td>
          <td><div align="center" class="MILfont-small">Coord4_long_deg</div></td>
          <td><div align="center" class="MILfont-small">Coord4_long_min</div></td>
        </tr>
        <tr valign="baseline">
          <td nowrap align="right">
            <div align="center" class="MILfont-small">
              <input type="text" name="coord4_lat_dir" value="N" size="5">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord4_lat_deg" value="" size="25">
            </div></td>
          <td>
            <div align="center" class="MILfont-small">
              <input type="text" name="coord4_lat_min" value="" size="25">
            </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord4_long_dir" value="W" size="5">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord4_long_deg" value="" size="25">
          </div></td>
          <td><div align="center" class="MILfont-small">
            <input type="text" name="coord4_long_min" value="" size="25">
          </div></td>
        </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center"><br>
          </div></td>
        </tr>
        <tr valign="baseline">
          <td colspan="6" align="right" nowrap><div align="center"></div>            <div align="center">
            <input type="submit" onClick="YY_checkform('form1','holding_id','#q','0','Field \'holding_id\' is not valid.','filed_by','#q','0','Field \'filed_by\' is not valid.','polygon_number','1_999','1','Polygon_number cannot be 0.');return document.MM_returnValue" value="Insert record">
            </div>            <div align="center"></div></td>
        </tr>
  </table>


</form>
    <p> </p>

	<?php
	include("../common_code/include_staff_footer.php");
	?>

	</body>
</html>
<?php
if (isset($_GET['id'])) {

}
?>

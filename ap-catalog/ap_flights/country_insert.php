<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

//* find number of rows currently in country_values table
//* then plus one to arbitrarily assign sort order number (user not define this at
//* this stage of development)
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM country_values ORDER BY country_sort_order ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$totalRows_Recordset1 = mysql_num_rows($Recordset1);


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
     $insertSQL = sprintf("INSERT INTO country_values (country_sort_order,country) VALUES (%s,%s)",
                      GetSQLValueString($_POST['country_sort_order'], "int"),
                      GetSQLValueString($_POST['country'], "text"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($insertSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $insertGoTo = "country_master.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert New Country Information</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />
<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILfont-x-large">Insert New Country <!-- /Sort Order --> <br /><br />
        </div></td>
  </tr>
</table>

<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">

  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="MILwhite MILcenter" >
  <col width="220" />
  <col width="320" />
    <tr><td>&nbsp;</td></tr>
    <tr valign="baseline">

<!-- not implemented at this stage
<tr valign="baseline">
      <td nowrap="nowrap" align="right" class="MILfont-edit">Sorting Order:</td>
      <td><input type="int" name="country_sort_order" value="" size="11" /></td>
    </tr>
-->
    <tr valign="baseline">
      <td nowrap="nowrap" align="right" class="MILfont-edit">Country:</td>
      <td><input type="text" name="country" value="" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Insert record" /></td>
    </tr>
  </table>

<?php
$country_sort_order = $totalRows_Recordset1 + 1;
?>
    <input type="hidden" name="country_sort_order" value="<?php echo $country_sort_order; ?>" />
  <input type="hidden" name="MM_insert" value="form1" />

</form>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->


<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>

<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {


//* format location checkbox input values for insertion into database
$input_chkLocation = $_REQUEST['chkLocation'];
$location = implode(" ",$input_chkLocation);
//* for debugging ... echo "values in the chkLocation array are $location";

  $updateSQL = sprintf("UPDATE ap_flights SET filed_by=%s, official_flight_id=%s, filed_by_in_catalog=%s, filed_by_in_collection=%s, location=%s, special_location=%s, scale_1=%s, scale_2=%s, scale_3=%s, index_type=%s, index_scale=%s, index_filed_under=%s, begin_date=%s, end_date=%s, `size`=%s, height=%s, width=%s, vertical=%s, oblique_high=%s, oblique_low=%s, overlap=%s, sidelap=%s, filmtype=%s, bw=%s, bw_IR=%s, color=%s, color_IR=%s, spectral_range=%s, printt=%s, pos_trans=%s, negative=%s, digital=%s,  roll=%s, cut_frame=%s, filter=%s, generation_held=%s, camera=%s, lens_focal_length=%s, platform_id=%s, directional_orientation=%s, altitude_a=%s, altitude_b=%s, altitude_c=%s, contractor_requestor=%s, flown_by=%s, acquired_from=%s, note=%s, copyright=%s, access_limitations=%s, area_general=%s, counties=%s, index_digital=%s, frames_scanned=%s, estimated_frame_count=%s, ready_ref=%s, prod_test=%s WHERE holding_id=%s",
                       GetSQLValueString($_POST['filed_by'], "text"),
                       GetSQLValueString($_POST['official_flight_id'], "text"),
                       GetSQLValueString($_POST['filed_by_in_catalog'], "text"),
                       GetSQLValueString($_POST['filed_by_in_collection'], "text"),
//                       GetSQLValueString($_POST['location'], "text"),
                       GetSQLValueString($location, "text"),
                       GetSQLValueString($_POST['special_location'], "text"),
                       GetSQLValueString($_POST['scale_1'], "int"),
                       GetSQLValueString($_POST['scale_2'], "int"),
                       GetSQLValueString($_POST['scale_3'], "int"),
                       GetSQLValueString($_POST['index_type'], "text"),
                       GetSQLValueString($_POST['index_scale'], "text"),
                       GetSQLValueString($_POST['index_filed_under'], "text"),
                       GetSQLValueString($_POST['begin_date'], "date"),
                       GetSQLValueString($_POST['end_date'], "date"),
                       GetSQLValueString($_POST['size'], "text"),
                       GetSQLValueString($_POST['height'], "double"),
                       GetSQLValueString($_POST['width'], "double"),
                       GetSQLValueString($_POST['vertical'], "int"),
                       GetSQLValueString($_POST['oblique_high'], "int"),
                       GetSQLValueString($_POST['oblique_low'], "int"),
                       GetSQLValueString($_POST['overlap'], "text"),
                       GetSQLValueString($_POST['sidelap'], "text"),
                       GetSQLValueString($_POST['filmtype'], "text"),
                       GetSQLValueString($_POST['bw'], "int"),
                       GetSQLValueString($_POST['bw_IR'], "int"),
                       GetSQLValueString($_POST['color'], "int"),
                       GetSQLValueString($_POST['color_IR'], "int"),
                       GetSQLValueString($_POST['spectral_range'], "text"),
                       GetSQLValueString($_POST['printt'], "int"),
                       GetSQLValueString($_POST['pos_trans'], "int"),
                       GetSQLValueString($_POST['negative'], "int"),
                       GetSQLValueString($_POST['digital'], "int"),
//*                       GetSQLValueString($_POST['o_p_d'], "text"),
                       GetSQLValueString($_POST['roll'], "int"),
                       GetSQLValueString($_POST['cut_frame'], "int"),
                       GetSQLValueString($_POST['filter'], "text"),
                       GetSQLValueString($_POST['generation_held'], "text"),
                       GetSQLValueString($_POST['camera'], "text"),
                       GetSQLValueString($_POST['lens_focal_length'], "text"),
                       GetSQLValueString($_POST['platform_id'], "text"),
                       GetSQLValueString($_POST['directional_orientation'], "text"),
                       GetSQLValueString($_POST['altitude_a'], "text"),
                       GetSQLValueString($_POST['altitude_b'], "text"),
                       GetSQLValueString($_POST['altitude_c'], "text"),
                       GetSQLValueString($_POST['contractor_requestor'], "text"),
                       GetSQLValueString($_POST['flown_by'], "text"),
                       GetSQLValueString($_POST['acquired_from'], "text"),
                       GetSQLValueString($_POST['note'], "text"),
                       GetSQLValueString($_POST['copyright'], "text"),
                       GetSQLValueString($_POST['access_limitations'], "text"),
                       GetSQLValueString($_POST['area_general'], "text"),
                       GetSQLValueString($_POST['counties'], "text"),
                       GetSQLValueString($_POST['index_digital'], "int"),
                       GetSQLValueString($_POST['frames_scanned'], "int"),
                       GetSQLValueString($_POST['estimated_frame_count'], "int"),
                       GetSQLValueString($_POST['ready_ref'], "text"),
                       GetSQLValueString($_POST['prod_test'], "text"),
                       GetSQLValueString($_POST['holding_id'], "int"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($updateSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $updateGoTo = "list_new.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_rs_area_general_values = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$rs_area_general_values = mysql_query($query_rs_area_general_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_rs_area_general_values = mysql_fetch_assoc($rs_area_general_values);
$totalRows_rs_area_general_values = mysql_num_rows($rs_area_general_values);

$colname_Recordset1 = "-1";
if (isset($_GET['holding_id'])) {
  $colname_Recordset1 = $_GET['holding_id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM ap_flights WHERE holding_id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

//* get all the stored location(s) for a specific holding_id for prepopulation of update screen data
$colname_location_id = $row_Recordset1['location'];
$location_id = (explode(" ",$colname_location_id));

//* get all the possible values for the location of the MIL collection
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);

include("../common_code/include_physical_fields_details.php");

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Update new AP Flight records</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style3 {font-size: large}
.style13 {
	font-size: small;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
</head>

<body>
<table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center" class="style3">
        <p>MIL Air Photo Flights Catalog Records</p>
        <p>Update filed_by: <?php echo $row_Recordset1['filed_by']; ?> holding_id: <?php echo $row_Recordset1['holding_id']; ?><br>
        </p>
    </div></td>
  </tr>
</table>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1">
  <table border="1" table width="57%" align="center">
    <tr valign="baseline">
      <td width="145" align="right" nowrap class="style13">Area_general:</td>
      <td width="371" class="style13"><select name="area_general" id="area_general">
          <?php
do {
?>
        <option value="<?php echo $row_rs_area_general_values['area_general']?>"<?php if (!(strcmp($row_rs_area_general_values['area_general'], $row_Recordset1['area_general']))) {echo "selected=\"selected\"";} ?>><?php echo $row_rs_area_general_values['area_general']?></option>
          <?php
} while ($row_rs_area_general_values = mysql_fetch_assoc($rs_area_general_values));
  $rows = mysql_num_rows($rs_area_general_values);
  if($rows > 0) {
      mysql_data_seek($rs_area_general_values, 0);
	  $row_rs_area_general_values = mysql_fetch_assoc($rs_area_general_values);
  }
?>
        </select></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13"><p>Counties:<br>
        (if Area_general = &quot;...Regions)</p>
      </td>
      <td class="style13"><input name="counties" type="text" id="counties" value="<?php echo $row_Recordset1['counties']; ?>" size="32"></td>
    </tr>

<?php
include("../common_code/include_ready_ref_yes_no.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13"><input name="holding_id2" type="hidden" id="holding_id2" value="<?php echo $row_Recordset1['holding_id']; ?>">
        Official_flight_id:</td>
      <td class="style13"><input type="text" name="official_flight_id" value="<?php echo $row_Recordset1['official_flight_id']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Filed_by:</td>
      <td class="style13"><input name="filed_by" type="text" id="filed_by" value="<?php echo $row_Recordset1['filed_by']; ?>" size="32"></td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="style13">Filed_by_in_catalog:</td>
      <td class="style13"><input type="text" name="filed_by_in_catalog" value="<?php echo $row_Recordset1['filed_by_in_catalog']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Filed_by_in_collection:</td>
      <td class="style13"><input type="text" name="filed_by_in_collection" value="<?php echo $row_Recordset1['filed_by_in_collection']; ?>" size="32"></td>
    </tr>

<?php
include("../common_code/include_location_checkboxes.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="style13">Imagery location:</td>
      <td class="style13"><input type="text" name="special_location" value="<?php echo $row_Recordset1['special_location']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Begin_date:</td>
      <td class="style13"><input type="text" name="begin_date" value="<?php echo $row_Recordset1['begin_date']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">End_date:</td>
      <td class="style13"><input type="text" name="end_date" value="<?php echo $row_Recordset1['end_date']; ?>" size="32"></td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="style13">Scale_1:</td>
      <td class="style13"><input type="text" name="scale_1" value="<?php echo $row_Recordset1['scale_1']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Scale_2:</td>
      <td class="style13"><input type="text" name="scale_2" value="<?php echo $row_Recordset1['scale_2']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Scale_3:</td>
      <td class="style13"><input type="text" name="scale_3" value="<?php echo $row_Recordset1['scale_3']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Index_type:</td>
      <td class="style13"><input type="text" name="index_type" value="<?php echo $row_Recordset1['index_type']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Index_scale:</td>
      <td class="style13"><input type="text" name="index_scale" value="<?php echo $row_Recordset1['index_scale']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Index_filed_under:</td>
      <td class="style13"><input type="text" name="index_filed_under" value="<?php echo $row_Recordset1['index_filed_under']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Size:</td>
      <td class="style13"><input type="text" name="size" value="<?php echo $row_Recordset1['size']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Height:</td>
      <td class="style13"><input type="text" name="height" value="<?php echo $row_Recordset1['height']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Width:</td>
      <td class="style13"><input type="text" name="width" value="<?php echo $row_Recordset1['width']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>

<?php
include("../common_code/include_physical_fields_details_display.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Overlap:</td>
      <td class="style13"><input type="text" name="overlap" value="<?php echo $row_Recordset1['overlap']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Sidelap:</td>
      <td class="style13"><input type="text" name="sidelap" value="<?php echo $row_Recordset1['sidelap']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Spectral_range:</td>
      <td class="style13"><input type="text" name="spectral_range" value="<?php echo $row_Recordset1['spectral_range']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Filter:</td>
      <td class="style13"><input type="text" name="filter" value="<?php echo $row_Recordset1['filter']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Generation_held:</td>
      <td class="style13"><input type="text" name="generation_held" value="<?php echo $row_Recordset1['generation_held']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Filmtype:</td>
      <td class="style13"><input type="text" name="filmtype" value="<?php echo $row_Recordset1['filmtype']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Camera:</td>
      <td class="style13"><input type="text" name="camera" value="<?php echo $row_Recordset1['camera']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Lens_focal_length:</td>
      <td class="style13"><input type="text" name="lens_focal_length" value="<?php echo $row_Recordset1['lens_focal_length']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Platform_id:</td>
      <td class="style13"><input type="text" name="platform_id" value="<?php echo $row_Recordset1['platform_id']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Directional_orientation:</td>
      <td class="style13"><input type="text" name="directional_orientation" value="<?php echo $row_Recordset1['directional_orientation']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Altitude_a:</td>
      <td class="style13"><input type="text" name="altitude_a" value="<?php echo $row_Recordset1['altitude_a']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Altitude_b:</td>
      <td class="style13"><input type="text" name="altitude_b" value="<?php echo $row_Recordset1['altitude_b']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Altitude_c:</td>
      <td class="style13"><input type="text" name="altitude_c" value="<?php echo $row_Recordset1['altitude_c']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13">&nbsp;</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Contractor_requestor:</td>
      <td class="style13"><input type="text" name="contractor_requestor" value="<?php echo $row_Recordset1['contractor_requestor']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Flown_by:</td>
      <td class="style13"><input type="text" name="flown_by" value="<?php echo $row_Recordset1['flown_by']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Acquired_from:</td>
      <td class="style13"><input type="text" name="acquired_from" value="<?php echo $row_Recordset1['acquired_from']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Note:</td>
      <td class="style13"><textarea name="note" cols="40" id="note"><?php echo $row_Recordset1['note']; ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Copyright:</td>
      <td class="style13"><input type="text" name="copyright" value="<?php echo $row_Recordset1['copyright']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Access_limitations:</td>
      <td class="style13"><input type="text" name="access_limitations" value="<?php echo $row_Recordset1['access_limitations']; ?>" size="32"></td>
    </tr>

<?php
include("../common_code/include_index_digital_yes_no.php");
?>
<?php
include("../common_code/include_frames_scanned_yes_no.php");
?>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">Estimated_frame_count:</td>
      <td class="style13"><input type="text" name="estimated_frame_count" value="<?php echo $row_Recordset1['estimated_frame_count']; ?>" size="32"></td>
    </tr>
<?php
include("../common_code/include_prod_test.php");
?>
    <tr valign="baseline">
      <td align="right" nowrap class="style13">&nbsp;</td>
      <td class="style13"><input type="submit" value="Update record"></td>
    </tr>
  </table>

<input type="hidden" name="holding_id" value="<?php echo $row_Recordset1['holding_id']; ?>">
<input type="hidden" name="MM_update" value="form1">
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rs_area_general_values);

mysql_free_result($Recordset1);
?>

<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM region_values ORDER BY region ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Frequently Requested Flights Region Master List</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>


<body class="MILlink">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="57%"  border="1" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><div align="center" class="MILfont-large">Region Table Maintenance<br>
        <table width="100%" border="0" cellpadding="5">
          <tr class="MILfont-small">
            <td><div align="left"><a href="index.php">Return to AP Flights Home</a></div></td>
            <td><div align="right"><a href="region_insert.php">Insert New Record</a></div></td>
          </tr>
        </table>
    </td>
  </tr>
</table>
<br />

<table border="1" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><strong>Region</strong></td>
    <td>&nbsp;</td>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_Recordset1['region']; ?>
      <input name="hiddenField" type="hidden" id="hiddenField" value="<?php echo $row_Recordset1['id']; ?>" /></td>
      <td><a href="region_update.php?id=<?php echo $row_Recordset1['id']; ?>">Edit</a></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>

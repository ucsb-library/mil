<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO ap_flights (filed_by, official_flight_id, filed_by_in_catalog, filed_by_in_collection, special_location, scale_1, scale_2, scale_3, index_type, index_scale, index_filed_under, `size`, height, width, vertical, oblique_high, oblique_low, overlap, sidelap, filmtype, bw, bw_IR, color, color_IR, spectral_range, printt, pos_trans, negative, digital, roll, cut_frame, filter, generation_held, camera, lens_focal_length, platform_id, directional_orientation, altitude_a, altitude_b, altitude_c, contractor_requestor, flown_by, acquired_from, note, copyright, access_limitations, area_general, counties, index_digital, frames_scanned, estimated_frame_count, ready_ref) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['filed_by'], "text"),
                       GetSQLValueString($_POST['official_flight_id'], "text"),
                       GetSQLValueString($_POST['filed_by_in_catalog'], "text"),
                       GetSQLValueString($_POST['filed_by_in_collection'], "text"),
                       GetSQLValueString($_POST['special_location'], "text"),
                       GetSQLValueString($_POST['scale_1'], "int"),
                       GetSQLValueString($_POST['scale_2'], "int"),
                       GetSQLValueString($_POST['scale_3'], "int"),
                       GetSQLValueString($_POST['index_type'], "text"),
                       GetSQLValueString($_POST['index_scale'], "text"),
                       GetSQLValueString($_POST['index_filed_under'], "text"),
                       GetSQLValueString($_POST['size'], "text"),
                       GetSQLValueString($_POST['height'], "double"),
                       GetSQLValueString($_POST['width'], "double"),
                       GetSQLValueString($_POST['vertical'], "int"),
                       GetSQLValueString($_POST['oblique_high'], "int"),
                       GetSQLValueString($_POST['oblique_low'], "int"),
                       GetSQLValueString($_POST['overlap'], "text"),
                       GetSQLValueString($_POST['sidelap'], "text"),
                       GetSQLValueString($_POST['filmtype'], "text"),
                       GetSQLValueString($_POST['bw'], "int"),
                       GetSQLValueString($_POST['bw_IR'], "int"),
                       GetSQLValueString($_POST['color'], "int"),
                       GetSQLValueString($_POST['color_IR'], "int"),
                       GetSQLValueString($_POST['spectral_range'], "text"),
                       GetSQLValueString($_POST['printt'], "int"),
                       GetSQLValueString($_POST['pos_trans'], "int"),
                       GetSQLValueString($_POST['negative'], "int"),
                       GetSQLValueString($_POST['digital'], "int"),
                       GetSQLValueString($_POST['roll'], "int"),
                       GetSQLValueString($_POST['cut_frame'], "int"),
                       GetSQLValueString($_POST['filter'], "text"),
                       GetSQLValueString($_POST['generation_held'], "text"),
                       GetSQLValueString($_POST['camera'], "text"),
                       GetSQLValueString($_POST['lens_focal_length'], "text"),
                       GetSQLValueString($_POST['platform_id'], "text"),
                       GetSQLValueString($_POST['directional_orientation'], "text"),
                       GetSQLValueString($_POST['altitude_a'], "text"),
                       GetSQLValueString($_POST['altitude_b'], "text"),
                       GetSQLValueString($_POST['altitude_c'], "text"),
                       GetSQLValueString($_POST['contractor_requestor'], "text"),
                       GetSQLValueString($_POST['flown_by'], "text"),
                       GetSQLValueString($_POST['acquired_from'], "text"),
                       GetSQLValueString($_POST['note'], "text"),
                       GetSQLValueString($_POST['copyright'], "text"),
                       GetSQLValueString($_POST['access_limitations'], "text"),
                       GetSQLValueString($_POST['area_general'], "text"),
                       GetSQLValueString($_POST['counties'], "text"),
                       GetSQLValueString($_POST['index_digital'], "int"),
                       GetSQLValueString($_POST['frames_scanned'], "int"),
                       GetSQLValueString($_POST['estimated_frame_count'], "int"),
                       GetSQLValueString($_POST['ready_ref'], "text"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($insertSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $insertGoTo = "list_new.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

$query_Recordset1 = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_year = "SELECT * FROM year_lookup ORDER BY `year` DESC";
$year = mysql_query($query_year, $MilWebAppsdb1mysql) or die(mysql_error());
$row_year = mysql_fetch_assoc($year);
$totalRows_year = mysql_num_rows($year);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_area_general_values = "SELECT area_general FROM area_general_values ORDER BY area_general ASC";
$area_general_values = mysql_query($query_area_general_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_area_general_values = mysql_fetch_assoc($area_general_values);
$totalRows_area_general_values = mysql_num_rows($area_general_values);
?>
<style type="text/css">
<!--
.style3 {font-size: large}
.style7 {color: #FF0000}
.style13 {
	font-size: x-small;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
<title>Insert new AP Flight records</title>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #999999;
}
.style14 {font-size: small}
-->
</style><table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center" class="style3">Insert New <span class="style7">Citipix</span> Flight Catalog Record </div></td>
  </tr>
</table>
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1">
  <table width="100%" border="2" cellspacing="1" bgcolor="#FFFFFF">
    <tr>
      <td width="50%" valign="top" class="style13"><table width="400"  border="1" align="left" cellpadding="5" bgcolor="#FFFFFF">
        <tr valign="baseline">
          <td colspan="3" align="right" nowrap class="style13">            <div align="left">
            <label>
            <select name="area_general" id="area_general">
              <option value="none">Please select an area (general):</option>
              <option value="Not listed, need to add">Not listed, need to add</option>
            </select>
            </label>
          </div></td>
          </tr>
        
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Ready Ref?</td>
          <td colspan="2" class="style13"><input name="ready_ref" type="text" id="ready_ref" value="no" size="5" /> 
            yes or no</td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Counties:</td>
          <td colspan="2" class="style13"><input name="counties" type="text" id="counties" size="32" />
            <br />
            Used to search for &quot;Flights by County&quot; when area_general = &quot;...Regions&quot;.<br />
            Example: San Diego; Orange; Los Angeles; Ventura; </td>
        </tr>
        <tr valign="baseline">
          <td width="27%" align="right" nowrap class="style13">Official <br>
            flight_id:</td>
          <td colspan="2" class="style13"><input type="text" name="official_flight_id" value="HM-" size="32">
            <br>
            Official full title of flight - first listed on <br>
            catalog record
            (called Flight I.D.)</td>
          </tr>
        
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filed_by</td>
          <td colspan="2" class="style13"><input name="filed_by" type="text" id="filed_by" value="HM-" size="32">
            <br>
            This field should only contain letters,<br>
            numbers and dashes (no other characters). </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filed_by<br>
            (in_catalog):</td>
          <td colspan="2" class="style13"><input type="text" name="filed_by_in_catalog" value="HM-" size="32">
            <br>
            Title flight is filed under in shelflist.</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13"><p>Filed_by<br>
            (in collection):</p>            </td>
          <td colspan="2" class="style13"><input type="text" name="filed_by_in_collection" value="HM-" size="32">
            <br>
            Title flight is filed under in MIL <br>
            collection 
            (usually the same as above, <br>
            except for PAI flights). </td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Special location:</td>
          <td colspan="2" class="style13"><input type="text" name="special_location" value="Room 2513" size="32">
            <br>
            Enter if imagery filed in unusual location: <br>
            Example: Roll, Film, Collection  </td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index type:</td>
          <td colspan="2" class="style13"><select name="index_type" id="index_type">
            <option value="none selected">Please select an index type:</option>
            <option value="mosaic">mosaic</option>
            <option value="line" selected="selected">line</option>
            <option value="spot">spot</option>
            <option value="spot">self-indexed</option>
            <option value="none">none</option>
            <option value="Not listed, need to add">Not listed, need to add</option>
                    </select></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index scale:</td>
          <td colspan="2" class="style13"><input name="index_scale" type="text" value="100000" size="32">
            <br>
             Example: 12000, 24000, 50000 </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index filed under:</td>
          <td colspan="2" class="style13"><input type="text" name="index_filed_under" value="HM-" size="32">
            <br>
            If index filed under alternative name, <br>
            list that name here.</td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Size:</td>
          <td colspan="2" class="style13"><input type="text" name="size" value="9 X 9 inches" size="32">
            <br>
            Example: frames 9 x 9 inches or frames 70mm </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Height:</td>
          <td colspan="2" class="style13"><input type="text" name="height" value="9" size="32">
            <br>
            List height in inches.</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Width:</td>
          <td colspan="2" class="style13"><input type="text" name="width" value="9" size="32">
            <br>
List width in inches.</td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" valign="top" nowrap>
            <div align="left">
              <table width="40%"  border="0" align="left">
                  <tr>
                    <td class="style13">&nbsp;</td>
                    <td class="style13"><div align="center">yes</div></td>
                    <td class="style13"><div align="center">no</div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Bw::</div></td>
                    <td class="style13"><div align="center">
                      <input name="bw" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="bw" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Bw IR::</div></td>
                    <td class="style13"><div align="center">
                      <input name="bw_IR" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="bw_IR" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Color:</div></td>
                    <td class="style13"><div align="center">
                      <input name="color" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="color" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Color IR:: </div></td>
                    <td class="style13"><div align="center">
                      <input name="color_IR" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="color_IR" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <td class="style13"><div align="right">Print::</div></td>
                  <td class="style13"><div align="center">
                    <input name="printt" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="printt" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Pos trans::</div></td>
                  <td class="style13"><div align="center">
                    <input name="pos_trans" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="pos_trans" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Negative::</div></td>
                  <td class="style13"><div align="center">
                    <input name="negative" type="radio" value="1" checked="checked" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="negative" type="radio" value="0" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Digital:: </div></td>
                  <td class="style13"><div align="center">
                    <input name="digital" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="digital" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Roll:</div></td>
                  <td class="style13"><div align="center">
                    <input name="roll" type="radio" value="1" checked="checked" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="roll" type="radio" value="0" />
                    </div></td>
                </tr>
              <tr>
              <td class="style13"><div align="right">Cut frame:</div></td>
              <td class="style13"><div align="center">
                <input name="cut_frame" type="radio" value="1" checked="checked" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="cut_frame" type="radio" value="0" />
              </div></td>
            </tr><tr>
              <td class="style13"><div align="right">Vertical:</div></td>
              <td class="style13"><div align="center">
                <input name="vertical" type="radio" value="1" checked="checked" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="vertical" type="radio" value="0" />
              </div></td>
            </tr>
            <tr>
              <td class="style13"><div align="right">Oblique high:</div></td>
              <td class="style13"><div align="center">
                <input name="oblique_high" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_high" type="radio" value="0" checked="checked" />
              </div></td>
            </tr>
            <tr>
              <td class="style13"><div align="right">Oblique low: </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_low" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_low" type="radio" value="0" checked="checked" />
              </div></td>
            </tr>
                </table>
            </div></td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
        </tr>        
        <tr valign="baseline">
          <td colspan="3" align="left" nowrap class="style13"><select name="copyright" id="copyright">
            <option value="none selected">Please select copyright information:</option>
            <option value="Reproduction rights held by the Regents of the University of California.">Reproduction rights held by the UC Regents.</option>
            <option value="Copyright �  UC Regents.  All Rights Reserved." selected="selected">Copyright �  UC Regents.  All Rights Reserved.</option>
            <option value="Copyright �  Pacific Western Aerial Surveys">Copyright �  Pacific Western Aerial Surveys</option>
            <option value="Copyright �  I.K. Curtis">Copyright �  I.K. Curtis</option>
            <option value="Copyright �  Air Photo USA">Copyright �  Air Photo USA</option>
            <option value="Copyright � WAC Corporation">Copyright � WAC Corporation</option>
            <option value="Copyright �  Hong Kong Government">Copyright �  Hong Kong Government</option>
            <option value="Copyright �  Real Estate Data Inc.">Copyright �  Real Estate Data Inc.</option>
            <option value="Copyright � Bud Kimball Photography">Copyright � Bud Kimball Photography</option>
            <option value="Copyright � Rupp Aerial Photography">Copyright � Rupp Aerial Photography</option>
            <option value="Copyright � California Real Estate and Zoning Aerial Survey">Copyright � California Real Estate and Zoning Aerial Survey</option>
            <option value="Not listed, need to add">Not listed, need to add</option>
          </select></td>
          </tr>
        <tr valign="baseline">
          <td colspan="3" align="right" nowrap class="style13"><div align="left">
            <select name="access_limitations" id="access_limitations">
              <option value="None selected">Please select access limitations:</option>
              <option value="None" selected="selected">None</option>
              <option value="UC only">UC only</option>
              <option value="See Staff">See Staff</option>
              <option value="Not listed, need to add">Not listed, need to add</option>
            </select>
          </div></td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index digital:</td>
          <td colspan="2" class="style13"><table width="150" border="0">
            <tr align="left" valign="middle">
              <td width="50%">yes
                <input name="index_digital" type="radio" value="1" checked="checked" /></td>
              <td width="50%">no
                <input name="index_digital" type="radio" value="0" /></td>
            </tr>
          </table>
            <br>
            Has the index been scanned? <br>
            Check with index scanning project.</td></tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Frames scanned:</td>
          <td colspan="2" class="style13"><table width="150" border="0">
            <tr align="left" valign="middle">
              <td width="50%">yes
                <input name="frames_scanned" type="radio" value="1" /></td>
              <td width="50%">no
                <input name="frames_scanned" type="radio" value="0" checked="CHECKED" /></td>
            </tr>
          </table>
            <br>
              Have all frames in the flight been scanned? <br>
            Check with ADL staff.</td></tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Estimated <br>
            frame count:</td>
          <td colspan="2" class="style13"><input type="text" name="estimated_frame_count" value="0" size="32">
              <br>
    Number of frames held <br>
    (put in estimate if exact # not known). </td>
        </tr>
      </table></td>
      <td width="50%" valign="top" class="style13"><table width="400"  border="1" cellpadding="5" bgcolor="#FFFFFF">
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Begin date:</td>
          <td class="style13"><span id="spryselect1">
          <span class="selectRequiredMsg">Please select a year.</br></span></span><span class="style14">
          <label>
          <select name="beg_year" id="beg_year">
            <option value="1999" selected="selected">1999</option>
            <option value="2000">2000</option>
            <option value="2001">2001</option>
            <option value="2002">2002</option>
          </select>
          </label>
          </span>
          <select name="beg_month" id="beg_month">
            <option value="01" selected="selected">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
          </select>
          <select name="beg_day" id="beg_day">
            <option value="01" selected="selected">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>
          </select></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">End date:</td>
          <td class="style13"><select name="end_year" id="end_year">
            <option value="1999" selected="selected">1999</option>
            <option value="2000">2000</option>
            <option value="2001">2001</option>
            <option value="2002">2002</option>
          </select>
            <select name="end_month" id="end_month">
              <option value="01" selected="selected">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
                        </select>
            <select name="end_day" id="end_day">
              <option value="01" selected="selected">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31">31</option>
                        </select></td>
        </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">First scale:</td>
          <td class="style13"><input type="text" name="scale_1" value="10800" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Second scale:</td>
          <td class="style13"><input type="text" name="scale_2" value="0" size="32">            </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Third scale:</td>
          <td class="style13"><input type="text" name="scale_3" value="0" size="32">            </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">&nbsp;</td>
          <td class="style13">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Overlap:</td>
          <td class="style13"><input type="text" name="overlap" value="60%" size="32">
            <br>
            Enter % overlap. Example:  60%. </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Sidelap:</td>
          <td class="style13"><input type="text" name="sidelap" value="30%" size="32">
            <br>
            Enter % sidelap. Example:  20%. </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Spectral range:</td>
          <td class="style13"><input type="text" name="spectral_range" value="350-700 nm" size="32">
            <br>
            Example: 510-900 nm. </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filter:</td>
          <td class="style13"><input type="text" name="filter" value="" size="32">
            <br>
            Example: Wratten 21. </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Generation held:</td>
          <td class="style13"><input type="text" name="generation_held" value="1st generation" size="32">
            <br>
            Example: 3rd generation or <br>            1st and 2nd generation. </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filmtype:</td>
          <td class="style13"><input type="text" name="filmtype" value="2444" size="32" />
            <br />
Example: Panchromatic or SO-397. </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Camera:</td>
          <td class="style13"><input type="text" name="camera" value="Zeiss RMK TOP 30" size="32">
            <br>
            Example: RC-10, # 76.   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Lens focal length:</td>
          <td class="style13"><input type="text" name="lens_focal_length" value="12 inches (305mm)" size="32">
            <br>
            Example: 12 inches 
            (304.8mm). </td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="2" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Platform id:</td>
          <td class="style13"><input type="text" name="platform_id" value="Cessna Conquest 441" size="32">
            <br>
            Example: U-2 Aircraft #5 .              </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Directional <br>
            orientation:</td>
          <td class="style13"><input type="text" name="directional_orientation" value="North-South" size="32">
            <br>
            Example: West-East; Northwest-Southeast             </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">&nbsp;</td>
          <td class="style13">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude a:</td>
          <td class="style13"><input type="text" name="altitude_a" value="10800" size="32">
            <br>
            Example: 65000 (feet are assumed, <br>
            convert if needed).   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude b:</td>
          <td class="style13"><input type="text" name="altitude_b" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude c:</td>
          <td class="style13"><input type="text" name="altitude_c" size="32"></td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Contractor <br>
            requestor:</td>
          <td class="style13"><input type="text" name="contractor_requestor" value="Kodak Global Imaging" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Flown by:</td>
          <td class="style13"><input type="text" name="flown_by" value="Hauts-Monts, Inc." size="32">
            <br>
            Example: U.S. Geological Survey</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Acquired from:</td>
          <td class="style13"><input type="text" name="acquired_from" value="Pacific Western Aerial Surveys" size="32">
            <br>
            Example: U.S. Forest Service or <br>            Landiscor Aerial Information.             </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" valign="top" nowrap class="style13">Note:<br>
            Enter all <br>
            supplemental<br>             
            information              </td>
          <td class="style13">              <textarea name="note" cols="30" rows="12" id="note">Citipix collection. Imagery acquired 2008.</textarea>             </td></tr>
      </table></td>
    </tr>
    <tr class="style13">
      <td height="105" colspan="2" valign="top" bgcolor="#FFFFFF"><div align="center">
        <p>&nbsp;          </p>
        <p>
          <input name="submit" type="submit" value="Insert AP Flights Catalog Record">
</p>
        <p>&nbsp;            </p>
      </div></td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="form1" />
</form>

<?php
mysql_free_result($Recordset1);

mysql_free_result($year);

mysql_free_result($area_general_values);
?>
<script type="text/javascript">
<!--
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
//-->
</script>

<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];
if (isset($_REQUEST['orderby']))  {
$orderby = $_REQUEST['orderby']; }
else {
$orderby = 'filed_by'; }
$maxRows_Recordset1 = 500;
$pageNum_Recordset1 = 0;
if (isset($_GET['pageNum_Recordset1'])) {
  $pageNum_Recordset1 = $_GET['pageNum_Recordset1'];
}
$startRow_Recordset1 = $pageNum_Recordset1 * $maxRows_Recordset1;

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
// *********** $query_Recordset1 = "SELECT * FROM coord_pairs_new WHERE deleted = 'no' ORDER BY $orderby ASC";
$query_Recordset1 = "SELECT * FROM coord_pairs_new ORDER BY $orderby ASC";
$query_limit_Recordset1 = sprintf("%s LIMIT %d, %d", $query_Recordset1, $startRow_Recordset1, $maxRows_Recordset1);
$Recordset1 = mysql_query($query_limit_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);

if (isset($_GET['totalRows_Recordset1'])) {
  $totalRows_Recordset1 = $_GET['totalRows_Recordset1'];
} else {
  $all_Recordset1 = mysql_query($query_Recordset1);
  $totalRows_Recordset1 = mysql_num_rows($all_Recordset1);
}
$totalPages_Recordset1 = ceil($totalRows_Recordset1/$maxRows_Recordset1)-1;

$queryString_Recordset1 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Recordset1") == false &&
        stristr($param, "totalRows_Recordset1") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Recordset1 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Recordset1 = sprintf("&totalRows_Recordset1=%d%s", $totalRows_Recordset1, $queryString_Recordset1);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>List Coord Pairs for MIL Air Photo Flights</title>
<style type="text/css">

<!--
.style3 {font-size: large}
.style10 {font-family: Arial, Helvetica, sans-serif; font-size: small; }
.style12 {font-family: Arial, Helvetica, sans-serif; font-size: x-small; font-weight: bold; }
.style13 {color: #FF0000; font-size: small; }
.style14 {font-size: small}
.style15 {font-family: Arial, Helvetica, sans-serif;}
body {
	background-color: #999999;
}
.style17 {font-size: small; font-family: Arial, Helvetica, sans-serif; color: #FFFFFF; }
.style18 {font-size: small}
-->
</style>
</head>
<body>


<A NAME="topScreen"></A>
<br>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td>
    <table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
	  <tr>
	    <td bgcolor="#FFFFFF"><div align="center" class="style3 style15">Coord Pairs for MIL Air Photo Flights</div>
	        <table width="100%" border="0" cellpadding="5">
	          <tr class="style14 style15">
	            <td><div align="left"><a href="index.php">Return to AP Flights Home</a></div></td>
	            <td><div align="right"><a href="#hereNow">Goto bottom of screen</a></div></td>
	          </tr>
	        </table>
	    </td>
	  </tr>
	</table>
		    </td>
	  </tr>
	  <tr><td>

	<table border="0" cellpadding="0" cellspacing="0"  align="right">
	  <tr>
	    <td align="right"><?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
	        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>"><img src="First.gif" alt="Go back to first page" border=0></a>
	        <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
	        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>"><img src="Previous.gif" alt="Go back one page" border=0></a>&nbsp;&nbsp;
	        <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
	        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>"><img src="Next.gif" alt="Go forward 1 page" border=0></a>
	        <?php } // Show if not last page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
	        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>"><img src="Last.gif" alt="Go to the last page" border=0></a>
	    <?php } // Show if not last page ?>    </td>
	  </tr>
	  <tr>
	    <td align="right"><span class="style17">Records <?php echo ($startRow_Recordset1 + 1) ?> to <?php echo min($startRow_Recordset1 + $maxRows_Recordset1, $totalRows_Recordset1) ?> of <?php echo $totalRows_Recordset1 ?></span> </td>
	  </tr>
	  <tr>
	    <td align="right"><span class="style17">
	     </span>
	      <form action="list_filed_by.php" method="post" name="form1" class="style17">
	            View records containing (filed_by):
	            <input name="filed_by" type="text" id="filed_by2">
	            <input type="submit" name="Submit" value="Go">
	      </form>
	     </td>
	  </tr>
</table>

    <!-- ********************** -->

    </td>
  </tr>

  <tr>
    <td><table border="1" bgcolor="#FFFFFF" align="center">
      <tr>
              <td><span class="style10">id for testing<br>ignore this Deborah</span></td>
        <td nowrap><span class="style10"><a href="list_coord_pairs_new.php?orderby=holding_id">holding_id</a><br>
          <span class="style10">(select title to sort) </span>        </span></td>
        <td nowrap><span class="style10"><a href="list_coord_pairs_new.php?orderby=filed_by">filed_by</a><br>
(select record to modify ) </span></td>

        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <?php do { ?>
      <tr>
        <td><span class="style10"><?php echo $row_Recordset1['id']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['holding_id']; ?></span></td>

<!--  take out linkability of filed_by filed
      <td><span class="style10"><a href="update_coord_pairs.php?id=<?php echo $row_Recordset1['id']; ?>"><?php echo $row_Recordset1['filed_by']; ?></a>
          <input name="id" type="hidden" id="id" value="<?php echo $row_Recordset1['id']; ?>">
        </span></td>
 -->

        <td><span class="style10"><?php echo $row_Recordset1['filed_by']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['deleted']; ?></span></td>

        <td><a href="update_coord_pairs_new.php?id=<?php echo $row_Recordset1['id']; ?>" class="style13">update</a></td>
      </tr>
      <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
    </table></td>
  </tr>
<tr><td>



<A NAME="hereNow"></A>

<table width=35% border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="left" class="style10">
      <a href="index.php">Return to AP Flights Home</a></td>
    <td bgcolor="#FFFFFF"><div align="right" class="style10"><a href="#topScreen">Goto top of screen</a></div></td>

  </tr>
</table>
</td></tr>
</table>

</body>
</html>

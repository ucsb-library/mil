<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "1";
if (isset($_POST['holding_id'])) {
  $colname_Recordset1 = $_POST['holding_id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT id, holding_id, filed_by, polygon_number FROM coord_pairs WHERE holding_id = %s ORDER BY polygon_number DESC", GetSQLValueString($colname_Recordset1, "-1"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$colname_Recordset_last_polygon_number = "99999911";
if (isset($_POST['holding_id'])) {
  $colname_Recordset_last_polygon_number = $_POST['holding_id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset_last_polygon_number = sprintf("SELECT id, holding_id, filed_by, polygon_number FROM coord_pairs WHERE holding_id = %s ORDER BY polygon_number DESC LIMIT 1", GetSQLValueString($colname_Recordset_last_polygon_number, "-1"));
$Recordset_last_polygon_number = mysql_query($query_Recordset_last_polygon_number, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset_last_polygon_number = mysql_fetch_assoc($Recordset_last_polygon_number);
$totalRows_Recordset_last_polygon_number = mysql_num_rows($Recordset_last_polygon_number);
?>

<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert Coord Pairs - AP Flights Cataloging</title>

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

</head>

<body class="MILlight-grey">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td ><div align="center" class="MILwhite MILfont-large">Coord Pairs for
      MIL Air Photo Flights</div></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>You have successfully updated a  record in the Coord Pairs table. </p>
<p> Here are all the polygons for that flight. If you want to view/edit one of these polygons, click on  the<em> polygon_number:</em></p>
<table border="1">
  <tr>
    <td>holding_id</td>
    <td>filed_by</td>
    <td>polygon_number</td>
  </tr>
  <?php do { ?>
  <tr>
    <td><?php echo $row_Recordset1['holding_id']; ?></td>
    <td><?php echo $row_Recordset1['filed_by']; ?>
    <input name="id" type="hidden" id="id" value="<?php echo $row_Recordset1['id']; ?>"></td>
    <td><a href="update_coord_pairs.php?id=<?php echo $row_Recordset1['id']; ?>"><?php echo $row_Recordset1['polygon_number']; ?></a></td>
  </tr>
  <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
<p>Or you can:</p>
<ul><li><a href="insert_coord_pairs2.php?id=<?php echo $row_Recordset_last_polygon_number['id']; ?>">Insert the next polygon for this flight</a> <br>
  </li>

  <!-- taken out as Deborah does not want this option when only dealing with adding co-ord
  pair data ... tooo confusing the work flow ... 07122010 mer
  <li><a href="insert_coord_pairs.php">Insert a new polygon for a new flight</a> <br>
</li>
  -->

  <li><a href="list_coord_pairs.php">View all Coord Pairs Records</a><br>
  </li>
  <li><a href="index.php">Return to Main Menu</a></li>
</ul>
<p>&nbsp;</p>

<p> </p>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($Recordset_last_polygon_number);
?>

<!DOCTYPE HTML>
<head>
<title>AP Flights Cataloging</title>
<?php
include("../common_code/include_MIL_style_links.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body class="MILlight-grey MILbody">
<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<table class="MILlink MILleft-padding-40 MILright-padding-40" width="100%" align="center" cellpadding="5" border="0">
  <tr>
    <td>

    <table border="0" class="MILwhite MILright-padding-15 menulist" width="100%" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#999999">
      <tr>
        <td class="MILleft-padding-40" valign="top"  width="40%">
          <p align="left" class="MILfont-large-bold">Cataloging: </p>
          <ul>
              <li>
                <a href="insert_form.php">Insert Draft Flight</a>
              </li>
              <li class="MILfont-medium"><a href="insert_model1.php">Insert Draft Flight - Model After Production Flight</a>
              </li>
              <li>
                <a href="list_new.php">Edit Draft AP Flights Records</a>
              </li>
              <li>
                <a href="list.php">Edit Production AP Flights Records</a>
              </li>
              <li>
                <a href="ready_ref_insert.php ">Insert Frequently Requested Flight</a>
              </li>
              <li><a href="ready_ref_master.php" class="MILfont-medium">Edit Frequently Requested Records</a>
              </li>
          </ul>
          </td>
          <td class="MILleft-padding-15" valign="top"  width="30%">

          <p align="left" class="MILfont-large-bold">Maintenance:</p>
          <ul>
              <li>
                <a href="location_insert.php">Insert New Location </a>
              </li>
              <li>
                <a href="location_master.php">Edit Location Information</a>
              </li>
              <li>
                <a href="country_insert.php">Insert New Country </a>
              </li>
              <li>
                <a href="country_master.php">Edit Country Information</a>
              </li>
          </ul>
          
		           </td>
          <td class="MILleft-padding-15" valign="top"  width="30%">

          <p align="left" class="MILfont-large-bold">Staff Only Reports:</p>
          <ul>
              <li>
                <a href="/apcatalog/ap_flights/staff_report/county.php?county_id=185&state_id=5&report_type=AllFlights&report_frames_scanned_type=1">All Digital Flights </a>
              </li>
              <li>
                <a href="/apcatalog/ap_flights/staff_report/county.php?county_id=185&state_id=5&report_type=AllFlights&report_frames_scanned_type=2">Partially Digital Flights</a>
              </li>
              <li>
                <a href="/apcatalog/ap_flights/staff_report/county.php?county_id=185&state_id=5&report_type=AllFlights&report_frames_scanned_type=0">Analog Only Flights </a>
              </li>
          </ul>
      </tr>
    </table>
    </td>
  </tr>

</table>
</td></tr></table>
</div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap z" -->
<?php
include("../common_code/include_staff_footer.php");
?>
</body>
</html>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Insert Draft Aerial Photography Catalog Record</title>


<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

$query_Recordset1 = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_existing_filed_by = "SELECT filed_by FROM ap_flights";
$existing_filed_by = mysql_query($query_existing_filed_by, $MilWebAppsdb1mysql) or die(mysql_error());
$row_existing_filed_by = mysql_fetch_assoc($existing_filed_by);
$totalRows_existing_filed_by = mysql_num_rows($existing_filed_by);
$results_existing_filed_by = array();
while ($row_existing_filed_by = mysql_fetch_assoc($existing_filed_by))
{
     $results_existing_filed_by[]=$row_existing_filed_by['filed_by'];
}
//print_r($results_existing_filed_by);
//phpinfo();

$query_year = "SELECT * FROM year_lookup ORDER BY `year` DESC";
$year = mysql_query($query_year, $MilWebAppsdb1mysql) or die(mysql_error());
$row_year = mysql_fetch_assoc($year);
$totalRows_year = mysql_num_rows($year);

$query_area_general_values = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$area_general_values = mysql_query($query_area_general_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_area_general_values = mysql_fetch_assoc($area_general_values);
$totalRows_area_general_values = mysql_num_rows($area_general_values);

$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);

$query_country_values = "SELECT * FROM country_values ORDER BY country_sort_order ASC";
$country_values = mysql_query($query_country_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_country_values = mysql_fetch_assoc($country_values);
$totalRows_country_values = mysql_num_rows($country_values);

$query_state_values = "SELECT * FROM state_values ORDER BY state_sort_order ASC";
$state_values = mysql_query($query_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_state_values = mysql_fetch_assoc($state_values);
$totalRows_state_values = mysql_num_rows($state_values);

$query_state_county_values = "SELECT county_values.county_id, county_values.county, county_values.state_id, county_values.state, county_values.country_id, county_values.country, state_values.state_sort_order from county_values, state_values where county_values.state_id = state_values.state_id order by state_values.state_sort_order, county_values.county ";
$state_county_values = mysql_query($query_state_county_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_state_county_values = mysql_fetch_assoc($state_county_values);
$totalRows_state_county_values = mysql_num_rows($state_county_values);

$numberUSAstates = '50';

?>

<?php
include("../common_code/include_MIL_style_links.php");
?>

<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />


<script type="text/javascript">


//* function to display/hide a section of html page

function unhide(divID) {
 var item = document.getElementById(divID);
 if (item) {
 item.className=(item.className=='hidden')?'unhidden':'hidden';
   }
 } //* end of function unhide(divID)


//* function to check all/uncheck all the options presented in a section of html page

function checkAll(checkAllStateID,tickAll) {
     var elem = document.getElementById(checkAllStateID);
     var tickAll = arguments[1];
 	 var inputs = elem.getElementsByTagName('input');

	 for (var i=0; i < inputs.length; i++)
		 {
			   if (tickAll == 'true') {
				   inputs[i].checked = true;
				   }
				   else
				   {
				  inputs[i].checked = false;
				   }
		 }
 } //* end of function checkAll(checkAllStateID,tickAll)


//* function to alert user when they uncheck a selected state, they are warned if there are
//* are still counties checked for the now unchecked state

function anythingChecked(checkAllStateID) {
     var elem = document.getElementById(checkAllStateID);
   	 var inputs = elem.getElementsByTagName('input');

     for (var i=0; i < inputs.length; i++)
     {
           if (inputs[i].checked) {
               inputs[i].checked = false;
               }

     }
   } //* end of function anythingChecked(checkAllStateID)


//* function to alert user that the new/draft flight that is being enetered in the database is not a new flight.

function checkIfUniqueFlight() {

var $input_string=document.form1.filed_by.value;
var $input_string_uppercase=$input_string.toUpperCase();

//alert("Information entered and uppercased" + $input_string_uppercase + " is here"); // for debugging purposes

// to get round the problem of php info being only server side and needing to do this check on the fly
// (ie: client side scripting), load up the exisiting php array of all the existing filed_by records into
// a javascript array so that the input from the form can be checked against it.
var js_array = <?php echo json_encode($results_existing_filed_by); ?>;

// find out if the input data already exists in the database;
// if it does, display a pop up message warning the user so

var isItThere = js_array.indexOf($input_string_uppercase);

// for debugging purposes
//alert ("the value of the index is  " + isItThere + " is here"); // return value -1 means not found

if (isItThere > 0) {
    alert ("Flight Filed By  " + $input_string_uppercase + " already exists. \n Please re-enter a new Filed By name");
    //document.form1.getElementById("filed_by").innerHTML = "";
    document.form1.filed_by.innerHTML = "";
    document.form1.filed_by.value = "";
//    document.getElementById('filed_by').focus();
//  document.form1.filed_by.focus();
//  document.form1.filed_by.select();

// the set Timeout is necessary for only Firefox which would NOT return the cursor to the correct
// input field if the user tabbed, or manually moved the cursor, out of the input field and there was
// an error (ie:duplicated filed_by flight entered)
// Firefox worked correctly if the user just used the carriage return
// Unbelievably, in this instance IE worked just fine with it all.
  if (navigator.appName != 'Microsoft Internet Explorer')
     {

//     alert("navigator name is " + navigator.appName);
		setTimeout(
			function()
			{
				var mytext = document.getElementById("filed_by");
				mytext.focus();
				mytext.select();
			}
		, 1); //* end of setTimeout
     }
     else
     {
     document.form1.filed_by.focus();
     } //* end of navigator.appName section

   }

} //* end of function checkIfUniqueFlight()

</script>

<!--  function to alert user in inappropriate type of NUMBER LETTER CHARACTER etc data has been entered -->
<script src="/apcatalog/common_code/include_data_integrity_check.js"></script>

<script type="text/javascript">

//* function to disable the return key (filched off the web)
//* reason for this is that users were able to hit the return key and it would submit the form garbage and all.

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
//-->
</script>

</head>
<body class="MILlight-grey">

<?php
include("../common_code/include_staff_header.php");
?>
<br />


<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILfont-x-large">Insert <span class="MILfont-bold">Draft</span> Aerial Photography Catalog Record <br />
        <span class="MILfont-red MILfont-edit">* </span><span class="MILfont-edit">Indicates required field</span></div></td>
  </tr>
</table>
<form action="insert_mysql.php" method="post" name="form1" onsubmit="MM_validateForm('official_flight_id','','R','filed_by','','R','filed_by_in_catalog','','R','filed_by_in_collection','','R');return document.MM_returnValue">


  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MILwhite">
    <tr>
      <td width="50%" valign="top" class="MILfont-edit MILleft-padding-40">

      <table width="480" border="1" align="left" cellspacing="0" cellpadding="5" bgcolor="#FFFFFF">

<tbody class="unhidden">

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Official <br>
            flight id:</td>
          <td width="310" class="MILfont-edit"><input class="MILfont-input-box" name="official_flight_id" type="text" id="official_flight_id" value="" size="32">
            <span class="MILfont-red">*</span><br>
            Official full title of flight - first listed on <br>
            catalog record
            (called Flight I.D.)</td>
          </tr>

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Filed by</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" name="filed_by" type="text" id="filed_by" value="" size="32" onblur="checkIfUniqueFlight(); checkDataInput(id,value,'numcharhyphen');">
            <span class="MILfont-red">*</span><br>
            This field should only contain letters,<br>
            numbers and dashes (no other characters). </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Filed by<br>
            (in catalog):</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" name="filed_by_in_catalog" type="text" id="filed_by_in_catalog" value="" size="32">
            <span class="MILfont-red">*</span><br>
            Title flight is filed under in shelflist.</td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit"><p>Filed by<br>
            (in collection):</p>            </td>
          <td class="MILfont-edit"><input class="MILfont-input-box" name="filed_by_in_collection" type="text" id="filed_by_in_collection" value="" size="32">
            <span class="MILfont-red">*</span><br>
            Title flight is filed under in MIL <br>
            collection
            (usually the same as above, <br>
            except for PAI flights). </td>
          </tr>

<!-- new section Country/State/County starts here -->
    <tr valign="baseline">
          <td width="143" align="right" valign="top" class="MILfont-edit">Country:</td>
		  <td width="310" class="MILfont-edit">

<?php

   $rows = mysql_num_rows($country_values);
   if($rows > 0 ) {
       mysql_data_seek($country_values, 0);
 	  $row_country_values = mysql_fetch_assoc($country_values);
 	  }

$default_USA = true;

//*************************************************

for ($i=0; $i<1; $i++)
  {

?>

<input type ="checkbox" name="chkCountry[]" value="<?php echo $row_country_values['country_id']?>" <?php  if ($default_USA) { ?> checked <?php ; $default_USA = false; } ?> ><?php echo $row_country_values['country']?>

<?php

	  $row_country_values = mysql_fetch_assoc($country_values);
	  $usa_country_id = $row_country_values['country_id'];
  } //* endof $i loop
?>

<br><span class="MILlink"><a href="javascript:unhide('displayAll<?php echo $row_country_values['country_id'];?>Countries');">Display all Countries</a></span>
<!-- div as here -->
<div id="displayAll<?php echo $row_country_values['country_id'];?>Countries" class="hidden">

<?php

  do {
?>
<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='unhide("display<?php echo $row_country_values['country_id'];?>Country");  anythingChecked("checkAll<?php echo $row_country_values['country_id'];?>StateProvince");'  name="chkCountry[]" value="<?php echo $row_country_values['country_id']?>" <?php  if ($default_USA) { ?> checked <?php ; $default_USA = false; } ?> ><?php echo $row_country_values['country']?></span>
              <?php
} while ($row_country_values = mysql_fetch_assoc($country_values));

?>

       </td>
    </tr>
</tbody>

<?php
	 $rows22 = mysql_num_rows($state_values);
     if($rows22 > 0 ) {
	    mysql_data_seek($state_values, $numberUSAstates);
	    $row_state_values = mysql_fetch_assoc($state_values);
		  }

$current_country = $row_state_values['country'];
$current_id = $row_state_values['country_id'];
$new_country = " ";
$current_country = chop($current_country);

for ($ii=0; $ii<($totalRows_state_values-$numberUSAstates); $ii++)
  {

if ($current_country != $new_country) { // ** new country found

	if ($ii==0) {
	$new_country = $current_country;
	}
	else {

?>
    </td>
  </tr>

  <!-- end of the tbody for each country -->
  </tbody >

	<?php
	} //* end of if ($ii==0)

$current_country = $new_country;
?>

  <tbody id="display<?php echo $row_state_values['country_id'];?>Country" class="hidden">
  <!-- beginning of the tbody for each country -->


  <tr valign="baseline" id ="checkAll<?php echo $row_state_values['country_id'];?>StateProvince">
     <td width="143" align="right" nowrap class="MILfont-edit"><?php echo $row_state_values['country']; echo ":" ?>
     </td>
     <td width="310" class="MILfont-edit MILlink"><a href="javascript:checkAll('checkAll<?php echo $row_state_values['country_id'];?>StateProvince', 'true');">Check all State/Province</a>
     &nbsp; &nbsp; <a href="javascript:checkAll('checkAll<?php echo $row_state_values['country_id'];?>StateProvince', 'false');">Uncheck all State/Province</a>
     </span>
     <br>
<?php
}  // ** end of new country found

?>

<input type ="checkbox" name="chkState[]" value="<?php echo $row_state_values['state_id']; echo "xx"; echo $row_state_values['country_id']; echo "xx"; echo $row_state_values['country_id']?>" ><?php echo $row_state_values['state'];?>

<?php

  $rows = mysql_num_rows($state_values);
  if($rows > 0 ) {
	  $row_state_values = mysql_fetch_assoc($state_values);
	  $new_country = $row_state_values['country'];
	  $new_country = chop($new_country);
  	  $new_county = $row_state_values['county'];
    }

} //* end of for ii loop

?>
<!--  i am here 4         </td>
       </tr>
       -->
 </div>
</tbody>

<tbody class="unhidden">
    <tr valign="baseline">
          <td width="143" align="right" valign="top" class="MILfont-edit">State:</td>
		  <td width="310"class="MILfont-edit">

<?php

  $rows = mysql_num_rows($state_values);
  if($rows > 0 ) {
      mysql_data_seek($state_values, 0);
	  $row_state_values = mysql_fetch_assoc($state_values);
	  }

for ($i=0; $i<=2; $i++)
  {

?>

<input type ="checkbox" onClick='unhide("display<?php echo $row_state_values['state_id'];?>Counties");  anythingChecked("checkAll<?php echo $row_state_values['state_id'];?>Counties");' name="chkState[]" value="<?php echo $row_state_values['state_id']; echo "xx";   echo $row_state_values['country_id']?>"><?php echo $row_state_values['state'];?>

<?php

	  $row_state_values = mysql_fetch_assoc($state_values);
	  $usa_country_id = $row_state_values['country_id'];
  } //* endof $i loop
?>

<br><span class="MILlink"><a href="javascript:unhide('displayAll<?php echo $row_state_values['country_id'];?>States');">Display all States</a></span>
<!-- div was here -->
<table id="displayAll<?php echo $row_state_values['country_id'];?>States" class="hidden" border="0">
<tr><td class="MILfont-edit">

<?php

  do {
   if ($usa_country_id == $row_state_values['country_id'])
	  { //* printout only united states of america states

	?>
<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='unhide("display<?php echo $row_state_values['state_id'];?>Counties"); anythingChecked("checkAll<?php echo $row_state_values['state_id'];?>Counties");' name="chkState[]" value="<?php echo $row_state_values['state_id']; echo "xx";   echo $row_state_values['country_id']?>"><?php echo $row_state_values['state']; ?></span>

	<?php
	 } //* end of printout only that country

 ?>

              <?php
} while ($row_state_values = mysql_fetch_assoc($state_values));
  $rows = mysql_num_rows($state_values);
  if($rows > 0 ) {
      mysql_data_seek($state_values, 0);
	  $row_state_values = mysql_fetch_assoc($state_values);
  }

?>
</td></tr></table>
<!-- </div> -->
          </td>
      </tr>

</tbody>

<!-- <div id="displaytest" style="shouldbedisplay: none"> -->

<?php
$current_state = $row_state_county_values['state'];
$current_id = $row_state_county_values['state_id'];
$new_state = " ";
$current_state = chop($current_state);

for ($ii=0; $ii<$totalRows_state_county_values; $ii++)
  {

if ($current_state != $new_state) { // ** new state found

	if ($ii==0) { //** only do for very first state
		$new_state = $current_state;
		}
		else {
		?>
			</td>
		  </tr>

		  <!-- end of the tbody for each state -->
		  </tbody >

		<?php
	} //** end of only do for very first state

$current_state = $new_state;
?>

  <tbody id="display<?php echo $row_state_county_values['state_id'];?>Counties" class="hidden">
  <!-- beginning of the tbody for each state -->

  <tr valign="baseline" id ="checkAll<?php echo $row_state_county_values['state_id'];?>Counties">
     <td width="143" align="right" nowrap class="MILfont-edit"><?php echo $row_state_county_values['state']; echo ":" ?>
     </td>
     <td width="310" class="MILfont-edit"><span class="MILlink"><a href="javascript:checkAll('checkAll<?php echo $row_state_county_values['state_id'];?>Counties', 'true');">Check all Counties</a>
     &nbsp; &nbsp; <a class="MILlink" href="javascript:checkAll('checkAll<?php echo $row_state_county_values['state_id'];?>Counties', 'false');">Uncheck all Counties</a>
     </span>
     <br>
<?php
}  // ** end of new state found

?>

<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" name="chkCounty[]" value="<?php echo $row_state_county_values['county_id']; echo "xx"; echo $row_state_county_values['state_id']; echo "xx"; echo $row_state_values['country_id']?>" ><?php echo $row_state_county_values['county'];?></span>

<?php
  $rows = mysql_num_rows($state_county_values);
   if($rows > 0 ) {
	  $row_state_county_values = mysql_fetch_assoc($state_county_values);
	  $new_state = $row_state_county_values['state'];
	  $new_state = chop($new_state);
  	  $new_county = $row_state_county_values['county'];
    }


} //* end of for ii loop

?>
           </td>
      </tr>
<!-- </div> -->
</tbody>

<tbody class="unhidden">
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Frequently Requested?</td>
		  <td width="310" align="left"class="MILfont-edit">
			  <input type="radio" name="ready_ref" value="yes" id="ready_ref"/> Yes
			  <input type="radio" name="ready_ref" value="no" id="ready_ref" checked/> No
		  </td>
        </tr>

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Begin date:</td>
          <td class="MILfont-edit"><span id="spryselect1">
          <span class="selectRequiredMsg MILfont-edit"">Please select a year.</br></span>
            <select name="beg_year" id="beg_year" class="MILfont-input-box">
              <option value="">Year</option>
              <?php
do {
?>
              <option value="<?php echo $row_year['year']?>"><?php echo $row_year['year']?></option>
              <?php
} while ($row_year = mysql_fetch_assoc($year));
  $rows = mysql_num_rows($year);
  if($rows > 0) {
      mysql_data_seek($year, 0);
	  $row_year = mysql_fetch_assoc($year);
  }
?>
            </select>
            </span>
            <select name="beg_month" id="beg_month" class="MILfont-input-box" >
              <option value="01" selected>01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select>
            <select name="beg_day" id="beg_day" class="MILfont-input-box" >
              <option value="01" selected>01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31">31</option>
            </select>
            <span class="MILfont-red">*</span></td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">End date:</td>
          <td class="MILfont-edit">              <select name="end_year" id="end_year" class="MILfont-input-box">
            <option value="none" <?php if (!(strcmp("none", "none"))) {echo "selected=\"selected\"";} ?>Year</option>
            <?php
do {
?><option value="<?php echo $row_year['year']?>"<?php if (!(strcmp($row_year['year'], "none"))) {echo "selected=\"selected\"";} ?>><?php echo $row_year['year']?></option><?php
} while ($row_year = mysql_fetch_assoc($year));
  $rows = mysql_num_rows($year);
  if($rows > 0) {
      mysql_data_seek($year, 0);
	  $row_year = mysql_fetch_assoc($year);
  }
?>
          </select>
            <select name="end_month" id="end_month" class="MILfont-input-box">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12" selected>12</option>
              </select>
            <select name="end_day" id="end_day" class="MILfont-input-box">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31" selected>31</option>
                        </select>
            <br>
              End date is not required if <br>
              flight
            has only one date. </td></tr>

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Scale:</td>
          <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_1" id="scale_1" value="" size="32" onblur="checkDataInput(id,value,'num');" >


            <br>
            Just enter number. Example: 12000 </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Scale:</td>
          <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_2" id="scale_2" value="" size="32" onblur="checkDataInput(id,value,'num');">            </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Scale:</td>
          <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_3" id="scale_3" value="" size="32" onblur="checkDataInput(id,value,'num');">            </td>
          </tr>

      <tr valign="baseline">
          <td width="143" valign="middle" align="right" nowrap class="MILfont-edit">Overlap:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="overlap" id="overlap" value="" size="32" onblur="checkDataInput(id,value,'numhyphen');">
            <br>
            Enter % overlap. Example:  60. </td>
          </tr>
        <tr valign="baseline">
          <td width="143" valign="middle" align="right" nowrap class="MILfont-edit">Sidelap:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="sidelap"id="sidelap" value="" size="32" onblur="checkDataInput(id,value,'numhyphen');">
            <br>
            Enter % sidelap. Example:  20. </td>
          </tr>

         <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Special location:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="special_location" value="" size="32">
            <br>
            Enter if imagery filed in unusual location: <br>
            Example: Annex 2 processing shelves </td>
          </tr>


		  <tr valign="baseline"">
		            <td width="143" valign="middle" align="right" nowrap class="MILfont-edit">Location:</td>
		  		  <td width="380" class="MILfont-edit">

		  <?php
		    do {
		  ?>
		  <li class="MILlist-inline MILright-padding-5"><input type ="checkbox" name="chkLocation[]" value="<?php echo $row_location_values['id']?>"><?php echo $row_location_values['location']; ?></li>
		                <?php
		  } while ($row_location_values = mysql_fetch_assoc($location_values));
		    $rows = mysql_num_rows($location_values);
		    if($rows > 0) {
		        mysql_data_seek($location_values, 0);
		  	  $row_location_values = mysql_fetch_assoc($location_values);
		    }
		  ?>
		   <!--           </select> -->


		            </td>
          </tr>

		  <tr valign="baseline">
			<td align="right" nowrap class="MILfont-edit" valign="middle">Frames scanned:</td>
			<td class="MILfont-edit">
			<table width="100%" border="0">
			  <tr align="left" valign="middle">
				<td width="20%" class="MILfont-edit">
				  <input name="frames_scanned" type="radio" value="1">Yes</td>
				<td width="20%" class="MILfont-edit">
				  <input name="frames_scanned" type="radio" value="0" checked>No</td>
				<td width="25%" class="MILfont-edit">
				  <input name="frames_scanned" type="radio" value="2">Some</td>
				<td width="35%" class="MILfont-edit">   &nbsp; </td>
				</td>
				<tr>
				<td width="100%" colspan="4" align="left" class="MILfont-edit">
				  <input name="frames_scanned" type="radio" value="3" /> Needs rescanning</td>
			  </tr>
			</table>
			</td>
        </tr>

        <tr valign="baseline">
          <td width="143" valign="middle" align="right" nowrap class="MILfont-edit">Index type:</td>
          <td class="MILfont-edit">
           <select name="index_type[]" id="index_type" multiple class="MILfont-input-box">
            <option value="none" selected="selected">Please select an index type:</option>
            <option value="mosaic">mosaic</option>
            <option value="line">line</option>
            <option value="spot">spot</option>
			<option value="spot">self-indexed</option>
			<option value="smartindex">SmartIndex</option>
			<option value="none">none</option>
			<option value="Not listed, see Notes.">Not listed, see Notes</option>
            </select></td>
          </tr>
       <tr valign="baseline">
          <td valign="middle" align="right" nowrap class="MILfont-edit">Index digital:</td>
          <td class="MILfont-edit">
          <table width="100%" border="0">
            <tr align="left" valign="middle">
              <td width="20%" class="MILfont-edit">
                <input name="index_digital" type="radio" value="1">Yes</td>
              <td width="80%" align="left" class="MILfont-edit">
                <input name="index_digital" type="radio" value="0" checked>No</td>
            </tr>
          </table>
          </td>
        </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Index Scale:</td>
          <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="index_scale" id="index_scale" value="" size="32" onblur="checkDataInput(id,value,'numspace');">
            <br>
             Example: 12000 24000 50000 </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Index filed under:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="index_filed_under" value="" size="32">
            <br>
            If index filed under alternative name, <br>
            list that name here.</td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Estimated <br>
            frame count:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="estimated_frame_count" id="estimated_frame_count" value="" size="32" onblur="checkDataInput(id,value,'num');">
              <br>
    Number of frames held <br>
    (put in estimate if exact # not known). </td>
        </tr>

       </table></td>

    <td width="50%" valign="top" class="MILfont-edit MILleft-padding-15">
      <table width="480"  border="1" cellpadding="5"cellspacing="0" bgcolor="#FFFFFF">

           <tr valign="baseline">
          <td width="143"align="right" nowrap class="MILfont-edit">Contractor <br>
            requestor:</td>
          <td width="336" class="MILfont-edit"><input class="MILfont-input-box" type="text" name="contractor_requestor" value="" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Flown by:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="flown_by" value="" size="32">
            <br>
            Example: U.S. Geological Survey</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Acquired from:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="acquired_from" value="" size="32">
            <br>
            Example: U.S. Forest Service or <br>            Landiscor Aerial Information.             </td>
          </tr>
        <tr valign="baseline">
          <td colspan="2" align="left" nowrap class="MILfont-edit"><select name="copyright" id="copyright" class="MILfont-input-box" >
            <option value="none" selected="selected">Please select copyright information:</option>
            <option value="Reproduction rights held by the Regents of the University of California.">Reproduction rights held by the UC Regents.</option>
            <option value="Copyright &copy;  UC Regents.  All Rights Reserved.">Copyright &copy; UC Regents. All Rights Reserved.</option>
            <option value="Copyright &copy;  Pacific Western Aerial Surveys">Copyright &copy; Pacific Western Aerial Surveys</option>
            <option value="Copyright &copy;  I.K. Curtis">Copyright &copy; I.K. Curtis</option>
            <option value="Copyright &copy;  Air Photo USA">Copyright &copy; Air Photo USA</option>
            <option value="Copyright &copy; WAC Corporation">Copyright &copy; WAC Corporation</option>
            <option value="Copyright &copy;  Hong Kong Government">Copyright &copy; Hong Kong Government</option>
            <option value="Copyright &copy;  Real Estate Data Inc.">Copyright &copy; Real Estate Data Inc.</option>
            <option value="Copyright &copy; Bud Kimball Photography">Copyright &copy; Bud Kimball Photography</option>
            <option value="Copyright &copy; Rupp Aerial Photography">Copyright &copy; Rupp Aerial Photography</option>
            <option value="Copyright &copy; California Real Estate and Zoning Aerial Survey">Copyright &copy; California Real Estate and Zoning Aerial Survey</option>
            <option value="Not listed, see Notes.">Not listed, see Notes.</option>
          </select></td>
          </tr>
        <tr valign="baseline">
          <td colspan="2" align="right" nowrap class="MILfont-edit"><div align="left">
            <select name="access_limitations" id="access_limitations" class="MILfont-input-box">
              <option value="none">Please select access limitations:</option>
              <option value="none">None</option>
              <option value="UC only">UC only</option>
              <option value="See Staff">See Staff</option>
              <option value="Not listed">Not listed, see Notes.</option>
            </select>
          </div></td>
          </tr>

<?php
include("../common_code/include_physical_fields_details_display.php");
?>

    <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Size:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="size" value="" size="32">
            <br>
            Example: frames 9 x 9 inches or frames 70mm </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Height:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="height" id="height" value="" size="32" onblur="checkDataInput(id,value,'numdot');">
            <br>
            List height in inches.</td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Width:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="width" id="width" value="" size="32" onblur="checkDataInput(id,value,'numdot');">
            <br>
List width in inches.</td>
          </tr>

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Generation held:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="generation_held" value="" size="32">
            <br>
            Example: 3rd generation or <br>            1st and 2nd generation. </td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Directional <br>
            orientation:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="directional_orientation" value="" size="32">
            <br>
            Example: West-East; Northwest-Southeast             </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Platform id:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="platform_id" value="" size="32">
            <br>
            Example: U-2 Aircraft #5 .              </td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Altitude:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_a" id="altitude_a" value="" size="32" onblur="checkDataInput(id,value,'num');">
            <br>
            Example: 65000 (feet are assumed, <br>
            convert if needed).   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Altitude:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_b" id="altitude_b" value="" size="32" onblur="checkDataInput(id,value,'num');"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Altitude:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_c" id="altitude_c" value="" size="32" onblur="checkDataInput(id,value,'num');"></td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Lens focal length:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="lens_focal_length" value="" size="32">
            <br>
            Example: 12 inches
            (304.8mm). </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Camera:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="camera" value="" size="32">
            <br>
            Example: RC-10, # 76.   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="MILfont-edit">Filmtype:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filmtype" value="" size="32" />
            <br />
            Example: Panchromatic or SO-397. </td>
        </tr>

        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Spectral range:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="spectral_range" value="" size="32">
            <br>
            Example: 510-900 nm. </td>
          </tr>
        <tr valign="baseline">
          <td width="143" align="right" nowrap class="MILfont-edit">Filter:</td>
          <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filter" value="" size="32">
            <br>
            Example: Wratten 21. </td>
          </tr>

        <tr valign="baseline">
          <td align="right" valign="top" nowrap class="MILfont-edit">Note:<br>
            Enter all <br>
            supplemental<br>
            information              </td>
          <td><textarea class="MILfont-input-box" name="note" cols="30" rows="12" id="note"></textarea>             </td></tr>
      </table></td>
    </tr>
    <tr>
      <td colspan=2 height="60" valign="middle" boder="none" bgcolor="#FFFFFF"><div align="center">
          <input name="submit" type="submit" class="MILfont-edit" value="Insert New Catalog Record">
      </div></td>
    </tr>

  </table>
<input type="hidden" name="area_general" value=" " />
</form>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>

<?php
mysql_free_result($Recordset1);

mysql_free_result($year);

mysql_free_result($area_general_values);

mysql_free_result($location_values);
?>
<script type="text/javascript">
<!--
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
//-->
</script>

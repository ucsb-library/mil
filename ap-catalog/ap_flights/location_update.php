<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE location_values SET sort_order=%s,location=%s WHERE id=%s",
                       GetSQLValueString($_POST['sort_order'], "int"),
                       GetSQLValueString($_POST['location'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($updateSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  $updateGoTo = "location_master.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM location_values WHERE id = %s", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit <?php echo $row_Recordset1['location']; ?> Information</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
     <td><div align="center" class="MILfont-x-large"> Edit Location Information<br /><br />
  </tr>
</table>
<br />

<form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">

  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="MILwhite MILcenter" >
  <col width="220" />
  <col width="320" />

    <tr><td>&nbsp;</td></tr>
    <tr valign="baseline">
      <td class="MILfont-edit" nowrap="nowrap" align="right">Id:</td>
      <td class="MILfont-input-box"><?php echo $row_Recordset1['id']; ?></td>
    </tr>
    <tr valign="baseline">
      <td class="MILfont-edit" nowrap="nowrap" align="right">Sorting Order:</td>
      <td ><input class="MILfont-input-box" type="int" name="sort_order" value="<?php echo htmlentities($row_Recordset1['sort_order'], ENT_COMPAT, 'utf-8'); ?>" size="3" /></td>
    </tr>
    <tr valign="baseline">
      <td class="MILfont-edit" nowrap="nowrap" align="right">Location:</td>
      <td><input class="MILfont-input-box" type="text" name="location" value="<?php echo htmlentities($row_Recordset1['location'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Update record" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id" value="<?php echo $row_Recordset1['id']; ?>" />
</form>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>

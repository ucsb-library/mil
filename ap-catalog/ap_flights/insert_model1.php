<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Insert Draft Flights Catalog Record Copied from Previous Record Entered</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body>

<div id="MILwrap">
   <div id="MILmain">


<?php
include("../common_code/include_staff_header.php");
?>
<br />

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="MILfont-medium" align="center">What flight would you like to use as a model?
<br><br>
           Enter <span class="MILfont-bold">filed by</span> and then Submit:</p>

<form id="form1" name="form1" method="post" action="insert_model2.php">
  <div align="center">
    <input class="MILfont-input-box" type="text" name="filed_by" id="filed_by" />
    <input class="MILfont-medium"" type="submit" name="model" id="model" value="Submit" />
  </div>
</form>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->

<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>

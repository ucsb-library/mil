<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MIL Area General Maintenance</title>

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

</head>

<body class=" MILlink">

<?php
include("../common_code/include_staff_header.php");
?>
<br />


<table class="MILheader-table MILwhite" width="57%" align="center" cellpadding="10" cellspacing="0">
  <tr>
    <td align="center" class="MILfont-x-large">Area General Table Maintenance<br>
    </td>
  <tr>
    <td class="MILfont-medium" align="left"><a href="index.php">Return to AP Flights Home</a>
    </td>
            <!--<td><div align="right"><a href="area_general_insert.php">Insert New Record</a></div></td>
            -->
  </tr>
</table>
<br />

<table class="MILtable" border="1" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td><strong>Area General</strong></td>
    <td>&nbsp;</td>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_Recordset1['area_general']; ?>
      <input name="hiddenField" type="hidden" id="hiddenField" value="<?php echo $row_Recordset1['id']; ?>" /></td>
      <td><a href="area_general_update.php?id=<?php echo $row_Recordset1['id']; ?>">Update</a></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>


<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>

<?php
mysql_free_result($Recordset1);
?>

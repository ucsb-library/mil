<?php require_once('../../Connections/MilWebAppsdb1mysql.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$implode_country = " ";
$implode_state = " ";
$implode_county = " ";

$colname_Recordset1 = "-1";
if (isset($_GET['filed_by'])) {
  $colname_Recordset1 = $_GET['filed_by'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM ap_flights WHERE filed_by = %s", GetSQLValueString($colname_Recordset1, "text"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$colname_Recordset1 = "-1";
if (isset($_GET['holding_id'])) {
  $colname_Recordset1 = $_GET['holding_id'];
}

$selected_holding = $row_Recordset1['holding_id'];

//* get all the countries for a particular holding_id
$query_holdings_country_values = sprintf("SELECT ap_flights_loc_country.country_id, country_values.country, country_values.country_sort_order FROM ap_flights_loc_country, country_values WHERE ap_flights_loc_country.holding_id = %s and ap_flights_loc_country.country_id = country_values.country_id order by country_values.country_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_country_values = mysql_query($query_holdings_country_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
$totalRows_holdings_country_values = mysql_num_rows($holdings_country_values);

//* get all the states for a particular holding_id
$query_holdings_state_values = sprintf("SELECT ap_flights_loc_state.country_id, ap_flights_loc_state.state_id, state_values.state, state_values.state_sort_order FROM ap_flights_loc_state, state_values WHERE ap_flights_loc_state.holding_id = %s and ap_flights_loc_state.state_id = state_values.state_id order by state_values.state_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_state_values = mysql_query($query_holdings_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
$totalRows_holdings_state_values = mysql_num_rows($holdings_state_values);

//**** get all the states for a particular holding_id
$query_holdings_state_values = sprintf("SELECT ap_flights_loc_state.country_id, ap_flights_loc_state.state_id, state_values.state, state_values.country_id, state_values.state_sort_order, country_values.country FROM ap_flights_loc_state, state_values, country_values WHERE ap_flights_loc_state.holding_id = %s and ap_flights_loc_state.state_id = state_values.state_id and ap_flights_loc_state.country_id = state_values.country_id and ap_flights_loc_state.country_id = country_values.country_id order by state_values.state_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_state_values = mysql_query($query_holdings_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
$totalRows_holdings_state_values = mysql_num_rows($holdings_state_values);

//**** get all the counties for a particular holding_id
 $query_holdings_county_values = sprintf("SELECT ap_flights_loc_county.country_id, ap_flights_loc_county.state_id, ap_flights_loc_county.county_id, county_values.county, state_values.state, county_values.county_sort_order FROM ap_flights_loc_county, county_values, state_values WHERE ap_flights_loc_county.holding_id = %s and ap_flights_loc_county.county_id = county_values.county_id and ap_flights_loc_county.state_id = state_values.state_id order by county_values.county_sort_order", GetSQLValueString($selected_holding, "int"));

 $holdings_county_values = mysql_query($query_holdings_county_values, $MilWebAppsdb1mysql) or die(mysql_error());
 $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
 $totalRows_holdings_county_values = mysql_num_rows($holdings_county_values);

//* get all country names associated with that holding id
//* set pointer to start of resultset array
	  $rows = mysql_num_rows($holdings_country_values);
	  if($rows > 0) {
 	      mysql_data_seek($holdings_country_values, 0);
      }

//* build string of all the country names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_country_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_country[] = $row['country']; // fetch each row...
      }

if (isset($resultset2_country)) {
	$implode_country = implode(", ",$resultset2_country);
//* 	echo "<br> yyyy ia m here country " . $implode_country . " xxxx <br>";
}

//* reset pointer to start of array for future reads
	  $rows2 = mysql_num_rows($holdings_country_values);
	  if($rows2 > 0) {
	      mysql_data_seek($holdings_country_values, 0);
	  $row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
	  }

//* get all state names associated with that holding id
//* set pointer to start of resultset array
	  $rows_state = mysql_num_rows($holdings_state_values);
	  if($rows_state > 0) {
 	      mysql_data_seek($holdings_state_values, 0);
      }

//* build string of all the state names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_state_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_state[] = $row['state']; // fetch each row...
      }

if (isset($resultset2_state)) {
	$implode_state = implode(", ",$resultset2_state);
}

//* reset pointer to start of array for future reads
	  $rows_state2 = mysql_num_rows($holdings_state_values);
	  if($rows_state2 > 0) {
	      mysql_data_seek($holdings_state_values, 0);
	  $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
	  }

//* get all county names associated with that holding id
//* set pointer to start of resultset array
	  $rows_county = mysql_num_rows($holdings_county_values);
	  if($rows_county > 0) {
 	      mysql_data_seek($holdings_county_values, 0);
      }

//* build string of all the county names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_county_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_county[] = $row['county']; // fetch each row...
      }

if (isset($resultset2_county)) {
	$implode_county = implode(", ",$resultset2_county);
//* echo "<br> xxxx iam here county " . $implode_county . "xxxx <br>";
}

//* reset pointer to start of array for future reads
	  $rows_county2 = mysql_num_rows($holdings_county_values);
	  if($rows_county2 > 0) {
	      mysql_data_seek($holdings_county_values, 0);
	  $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
	  }

//* get all the stored location(s) for a specific holding_id for prepopulation of reporting screen data
$colname_location_id = $row_Recordset1['location'];
$location_id = (explode(" ",$colname_location_id));

//* get all the possible values for the location of the MIL collection
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);

include("../../common_code/include_physical_fields_details.php");

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>

<?php
include("../../common_code/include_MIL_style_links.php");
?>

<title>Flight <?php echo $row_Recordset1['filed_by']; ?></title>

</head>

<body class="MILlink">

<?php
include("../../common_code/include_staff_header.php");
?>

<table class="MILleft-margin-40 MILtop-margin-10" width="650" align="left" cellpadding="10" cellspacing="0">
  <tr>
    <td align="center" ><table  border="1" width="72%" align="center" cellpadding="10" cellspacing="0">
      <tr>
        <td align="center"><div class="MILfont-large-bold">Imagery Report: Flight <?php echo $row_Recordset1['filed_by']; ?>

        <span class="MILfont-medium">
        <input type="hidden" name="hiddenField">
       	<?php if ($row_Recordset1['frames_scanned'] == 1)  {  ?>
		<span class="MILfont-red"><br>Digital</span>
		<?php ; } ?>
		<?php if ($row_Recordset1['frames_scanned'] == 2)  {  ?>
		<span class="MILfont-red"><br>Partially Digital</span>
		<?php ; } ?>
		<?php if ($row_Recordset1['frames_scanned'] == 3)  {  ?>
		<span class="MILfont-small">Needs to be Rescanned</span>
		<?php ; } ?>
		<br>
        <span class="MILfont-medium MILlink">
		<?php
		if ($row_Recordset1['index_digital'] == 1) {
			$flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
			$flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
			$flight_id_lower = strtolower($flight_id_cleaner);
		?>
			<a href="http://mil.library.ucsb.edu/ap_indexes/<?php echo $flight_id_lower; ?>/">View Index</a>
		</span>
				</div>
				</td></tr>
		<?php
		}
		  if ($row_Recordset1['index_digital'] == 0)  { ?>
          Index: Ask MIL Staff</span></td>
			</tr>
	   <?php
	   } ;
	   ?>

    </table></td>
  </tr>
  <tr>
    <td><table width="960" border="0" align="left" cellpadding="5" cellspacing="0">
      <tr valign="top">
        <td width="33%" valign="top" class="MILfont-small">

         <table width="320" align="left" border="1" cellpadding="5" bgcolor="#FFFFFF" cellspacing="0" cellpadding="5">

            <tr width=valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Country:</td>
              <td width="50%" colspan="2" class="MILfont-small"><?php echo $implode_country; ?> </td>
            </tr>

<!-- **** beginning of now display all the states in all the countries **** -->

           <tr valign="baseline">
              <td valign="top" align="right" class="MILfont-small MILfont-bold" >State(s):</td>
              <td valign="top" width="50%" colspan="2" class="MILfont-small">

  <?php


  //* reset pointer to start of array for future reads

  	 $rows22 = mysql_num_rows($holdings_state_values);
       if($rows22 > 0 ) {
          mysql_data_seek($holdings_state_values, 0);
          $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
  		  }

  $current_country = $row_holdings_state_values['country'];
  $current_country_id = $row_holdings_state_values['country_id'];

  $new_country = "abc";
  $current_country = chop($current_country);
  $another_new_country = false;

  if ($totalRows_holdings_state_values>0) { //* records found for states

  for ($jj=0; $jj<$totalRows_holdings_state_values; $jj++)
    {  //* beginning of jj LOOP statement

     if ($current_country != $new_country or $new_country == "") { // ** new country found or end of read in holdings

        if ($jj==0) {

             if ($totalRows_holdings_country_values>1) {

			 echo $row_holdings_state_values['country'];
			 echo ": <br>";
			 }
         	 ?>
			 <table frame=void margin=0 valign="top" cellspacing="0" cellpadding="0" border=0>
				<tr>
				<?php
				if ($totalRows_holdings_country_values>1) {
                 ?>
                 <td class="MILleft-padding-15 MILfont-small">
                 <?php
                 }
                 else
                 {
                 ?>
                 <td class="MILfont-small">
                 <?php
                 }

//*			 $current_country = $new_country;
			 }
			 else {
			 ?>
			</td></tr></table>
			<?php
			 echo $row_holdings_state_values['country'];
			 echo ": <br>";
        	 ?>
			 <table border="0">
				<tr> <td class="MILleft-padding-15 MILfont-small">
<?php
            $current_country = $new_country;
			}
       }

?>
<span class="nobreak">
<?php
echo chop($row_holdings_state_values['state']);

$rows = mysql_num_rows($holdings_state_values);
if($rows > 0 ) {
	  $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
	  $new_country = $row_holdings_state_values['country'];
	  $new_country_id = $row_holdings_state_values['country_id'];
	  $new_country = chop($new_country);
	  if ($current_country == $new_country) {
	  echo ",";
	     }
	  }
?>
</span>
<?php
    } //* end of jj loop
?>

    </td></tr></table>

<?php
} //* end of records found for states
?>
  </td></tr>

<!-- **** end of now display all the states in all the countries **** -->


<!-- **** beginning of now display all the counties in all the states **** -->

           <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Counties:</td>
              <td width="50%" colspan="2" class="MILfont-small">

  <?php
  if ($totalRows_holdings_county_values==0) {
  echo " &nbsp;";
  }

  //* reset pointer to start of array for future reads

  	 $rows333 = mysql_num_rows($holdings_county_values);
       if($rows333 > 0 ) {
          mysql_data_seek($holdings_county_values, 0);
          $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
  		  }

  $current_state = $row_holdings_county_values['state'];
  $current_state_id = $row_holdings_county_values['state_id'];

  $new_state = "abc";
  $current_state = chop($current_state);
  $another_new_state = false;

  if ($totalRows_holdings_county_values>0) { //* records found for counties

  for ($jj=0; $jj<$totalRows_holdings_county_values; $jj++)
    {  //* beginning of jj LOOP statement

     if ($current_state != $new_state or $new_state == "") { // ** new state found or end of read in holdings

        if ($jj==0) {

			 echo $row_holdings_county_values['state'];
			 echo ": <br>";
         	 ?>
			 <table border="0">
				<tr> <td class="MILleft-padding-15 MILfont-small">
			 <?php
			 }
			 else {
			 ?>
			</td></tr></table>
			<?php
			 echo $row_holdings_county_values['state'];
			 echo ": <br>";
        	 ?>
			 <table border="0">
				<tr> <td class="MILleft-padding-15 MILfont-small">
<?php
             $current_state = $new_state;
			}
       }

?>
<span class="nobreak">
<?php
echo chop($row_holdings_county_values['county']);
?>
<?php
$rows = mysql_num_rows($holdings_county_values);
if($rows > 0 ) {
	  $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
	  $new_state = $row_holdings_county_values['state'];
	  $new_state_id = $row_holdings_county_values['state_id'];
	  $new_state = chop($new_state);
	  if ($current_state == $new_state) {
	  echo",";
	     }
	  }
?>
</span>
<?php
    } //* end of jj loop
?>

</td></tr></table>

<?php
} //* end of records found for counties
?>
  </td></tr>

<!-- **** end of now display all the counties in all the states **** -->

            <tr valign="baseline">
              <td align="right" class="MILfont-light-grey MILfont-small MILfont-bold" >Area-general (deprecated):</td>
              <td width="50%" colspan="2" class="MILfont-light-grey MILfont-small"><?php echo $row_Recordset1['area_general']; ?>&nbsp; </td>
            </tr>

           <?php if ($row_Recordset1['counties'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="MILfont-light-grey MILfont-small MILfont-bold" >Counties (deprecated):</td>
              <td colspan="2" class="MILfont-light-grey MILfont-small"><?php echo $row_Recordset1['counties']; ?></td>
            </tr>
            <?  } ; ?>


<!--  //********************************** -->
<!--            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >State(s):</td>
              <td width="50%" colspan="2" class="MILfont-small"><?php echo $implode_state; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Counties:</td>
              <td colspan="2" class="MILfont-small"><?php echo $implode_county; ?></td>
            </tr>
-->



            <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Official flight id:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['official_flight_id']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Filed by
            (catalog):</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['filed_by_in_catalog']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Filed by
              (collection):</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['filed_by_in_collection']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Imagery Location:</td>
              <td colspan="2" class="MILfont-small">
            <?php
include("../../common_code/include_location_details.php");
                 ?>
              </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-light-grey MILfont-small MILfont-bold" >Special Location (deprecated):</td>
              <td colspan="2" class="MILfont-light-grey MILfont-small"><?php echo $row_Recordset1['special_location']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Index type:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['index_type']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Index scale:</td>
              <td colspan="2" class="MILfont-small"><?php
			  if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['index_scale']))
			  {
			     // all digits so display comma delineated
			     echo "1:".number_format($row_Recordset1['index_scale']);
			  }
			  else
			  {
			     // not all digits so just show the field as it is
			     echo "1:".$row_Recordset1['index_scale'];
			  }
			  ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Index filed under:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['index_filed_under']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Size:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['size']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Height:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['height']; ?> </td>
            </tr>

			  <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Width:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['width']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Frames scanned:</td>
              <td class="MILfont-small">
				<?php if ($row_Recordset1['frames_scanned'] == 1)  {  ?>
				<span class="MILfont-small">Digital</span>
				<?php ; } ?>
				<?php if ($row_Recordset1['frames_scanned'] == 2)  {  ?>
				<span class="MILfont-small">Partially Digital</span>
				<?php ; } ?>
				<?php if ($row_Recordset1['frames_scanned'] == 3)  {  ?>
				<span class="MILfont-small">Needs to be Rescanned</span>
				<?php ; } ?>
				<?php if ($row_Recordset1['frames_scanned'] == 0)  {  ?>
				<span class="MILfont-small">No</span>
				<?php ; } ?>
              </td>
            </tr>


<!--            <tr valign="baseline" class="MILfont-small">
              <td colspan="3" align="right" valign="top" class="MILfont-small" >
                <div align="left"></div></td>
            </tr>
-->

        </table></td>
        <td width="33%" valign="top" class="MILfont-small">

        <table width="320" border="1" bgcolor="#FFFFFF" cellspacing="0" cellpadding="5">

            <tr valign="baseline">
              <td width="50%" align="right" class="MILfont-small MILfont-bold" >Begin date:</td>
               <?php
				// convert mysql date to php timestamp
				$phptimestamp = strtotime( $row_Recordset1['begin_date'] );
				// now format php timestamp
				$begin_date = date( 'Y-m-d ', $phptimestamp );
				?>
    		 <td  nowrap><div align="left" class="MILfont-small"><?php echo $begin_date; ?></div> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold">End date:</td>
               <?php
				// convert mysql date to php timestamp
				$phptimestamp = strtotime( $row_Recordset1['end_date'] );
				// now format php timestamp
				$end_date = date( 'Y-m-d ', $phptimestamp );
				?>
    		 <td nowrap><div align="left" class="MILfont-small"><?php echo $end_date; ?></div> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Scale:</td>
              <td class="MILfont-small"><?php echo "1:".number_format($row_Recordset1['scale_1']); ?> </td>
            </tr>

              <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Scale:</td>
              <td class="MILfont-small"><?php echo "1:".number_format($row_Recordset1['scale_2']); ?>  </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Scale:</td>
              <td class="MILfont-small"><?php echo "1:".number_format($row_Recordset1['scale_3']); ?>  </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Overlap:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['overlap']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Sidelap:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['sidelap']; ?> </td>
            </tr>

           <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Directional orientation:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['directional_orientation']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Platform id:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['platform_id']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Altitude:</td>
              <td class="MILfont-small"><?php
				if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_a']))
				{
				   // all digits so display comma delineated
				   echo number_format($row_Recordset1['altitude_a']);
				}
				else
				{
				   // not all digits so just show the field as it is
				   echo $row_Recordset1['altitude_a'];
				}
				?>
				</td>
			</tr>

            <tr valign="baseline">
			  <td  align="right" class="MILfont-small MILfont-bold">Altitude:</td>
			  <td class="MILfont-small"><?php
				if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_b']))
				{
				   // all digits so display comma delineated
				   echo number_format($row_Recordset1['altitude_b']);
				}
				else
				{
				   // not all digits so just show the field as it is
				   echo $row_Recordset1['altitude_b'];
				}
				?>
     			</td>
     		</tr>

			<tr valign="baseline">
			  <td  align="right" class="MILfont-small MILfont-bold">Altitude:</td>
			  <td class="MILfont-small"><?php
				if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_c']))
				{
				   // all digits so display comma delineated
				   echo number_format($row_Recordset1['altitude_c']);
				}
				else
				{
				   // not all digits so just show the field as it is
				   echo $row_Recordset1['altitude_c'];
				}
				?>
    			</td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Lens focal length:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['lens_focal_length']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Camera:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['camera']; ?> </td>
            </tr>


            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Film type:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['filmtype']; ?> </td>
            </tr>

             <tr valign="baseline">
               <td  align="right" class="MILfont-small MILfont-bold">Spectral range:</td>
               <td class="MILfont-small"><?php echo $row_Recordset1['spectral_range']; ?> </td>
            </tr>


            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Filter:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['filter']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Generation held:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['generation_held']; ?> </td>
            </tr>

            <?php if ($row_Recordset1['ready_ref'] == 'yes' )  {  ?>
            <tr valign="baseline">
              <td align="right" class="MILfont-small  MILfont-bold" >Frequently Requested:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['ready_ref']; ?>
              </td>
              </tr>
             <?  } ; ?>

        </table></td>
        <td width="33%" valign="top" class="MILfont-small">

        <table width="320" border="1" cellpadding="5" bgcolor="#FFFFFF" cellspacing="0" cellpadding="5">

            <tr align="left" valign="baseline">
              <td colspan="2" valign="top" class="MILfont-small" ><div align="left"><span class="MILfont-bold">Note:</span>
                <table  border="0">
                <tr class="MILfont-small">
                  <td class="MILfont-small"><?php echo $row_Recordset1['note']; ?></td>
                </tr>
                </table></div>              </td>
            </tr>

             <tr valign="baseline">
               <td width="50%" align="right" class="MILfont-small MILfont-bold" >Physical Details:</td>
               <td colspan="2" class="MILfont-small">
               	      <span id='span1'><?php echo $bw_describe; ?></span>
 			  	      <span id='span2'><?php echo $bw_IR_describe; ?></span>
 			  	      <span id='span3'><?php echo $color_describe; ?></span>
 			  	      <span id='span4'><?php echo $color_IR_describe; ?></span>
 			  	      <span id='span5'><?php echo $printt_describe; ?></span>
 			  	      <span id='span6'><?php echo $pos_trans_describe; ?></span>
 			  	      <span id='span7'><?php echo $negative_describe; ?></span>
 			  	      <span id='span8'><?php echo $digital_describe; ?></span>
 			  	      <span id='span9'><?php echo $roll_describe; ?></span>
 			  	      <span id='span10'><?php echo $cut_frame_describe; ?></span>
 			  	      <span id='span11'><?php echo $vertical_describe; ?></span>
 			  	      <span id='span12'><?php echo $oblique_high_describe; ?></span>
 	                  <span id='span13'><?php echo $oblique_low_describe; ?></span>
                    &nbsp;</td>
             </tr>

             <tr valign="baseline">
               <td align="right" class="MILfont-light-grey MILfont-small MILfont-bold" >Other Physical Details (deprecated):</td>
               <td colspan="2" class="MILfont-light-grey MILfont-small"><?php echo $row_Recordset1['o_p_d']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Copyright:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['copyright']; ?>  </td>
            </tr>


            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Access limitations:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['access_limitations']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Flown by:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['flown_by']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Contractor/requestor:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['contractor_requestor']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold">Acquired from:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['acquired_from']; ?></td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Est. frame count:</td>
              <td colspan="2" class="MILfont-small"><?php echo $row_Recordset1['estimated_frame_count']; ?> </td>
            </tr>

            <tr valign="baseline">
              <td align="right" class="MILfont-small MILfont-bold" >Production/Test:</td>
              <td class="MILfont-small" ><?php echo $row_Recordset1['prod_test']; ?></td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold"> MIL Holding ID:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['holding_id']; ?></td>
            </tr>

            <tr valign="baseline">
              <td  align="right" class="MILfont-small MILfont-bold"> Last Update:</td>
              <td class="MILfont-small"><?php echo $row_Recordset1['last_update']; ?></td>
            </tr>

         </table></td>
      </tr>
    </table>
   </td>
  </tr>
</table>

<?php
include("../../common_code/include_staff_footer.php");
?>
</body>
</html>
<?php
mysql_free_result($Recordset1);

?>

<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
$currentPage = $_SERVER["PHP_SELF"];
@mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

// format certain fields before inserting into db

// format begin and end dates from year, month and day fields
$begin_date = $_REQUEST['beg_year'] . "-" . $_REQUEST['beg_month'] . "-" . $_REQUEST['beg_day'];
if ($_REQUEST['end_year'] == "none") {
  $end_date = $begin_date; }
else
  {$end_date = $_REQUEST['end_year'] . "-" . $_REQUEST['end_month'] . "-" . $_REQUEST['end_day'];}

//format index_type field

//* get all the location(s) user input for a specific holding_id
if (isset($_REQUEST['chkLocation']))  {
	$input_chkLocation = $_REQUEST['chkLocation'];
	$location = implode(" ",$input_chkLocation);
	//* for debugging ... echo "values in the chkLocation array are $location";
}

$selected_index_type = $_REQUEST['index_type'];
$index_type = implode("; ",$selected_index_type);

// format opd
if ($_REQUEST['bw']) {
  $bw = "Black and White; "; }
else
  {$bw = ""; }
if ($_REQUEST['bw_IR']) {
  $bw_IR = "Black and White IR; "; }
else
  {$bw_IR = ""; }
if ($_REQUEST['color']) {
  $color = "Color; "; }
else
  {$color = ""; }
if ($_REQUEST['color_IR']) {
  $color_IR = "Color IR; "; }
else
  {$color_IR = ""; }
if ($_REQUEST['printt']) {
  $printt = "Print; "; }
else
  {$printt = ""; }
if ($_REQUEST['pos_trans']) {
  $pos_trans = "Pos trans; "; }
else
  {$pos_trans = ""; }
if ($_REQUEST['negative']) {
  $negative = "Negative; "; }
else
  {$negative = ""; }
if ($_REQUEST['digital']) {
  $digital = "Digital; "; }
else
  {$digital = ""; }
if ($_REQUEST['roll']) {
  $roll = "Roll; "; }
else
  {$roll = ""; }
if ($_REQUEST['cut_frame']) {
  $cut_frame = "Cut frame; "; }
else
  {$cut_frame = ""; }
if ($_REQUEST['vertical']) {
  $vertical = "Vertical; "; }
else
  {$vertical = ""; }
if ($_REQUEST['oblique_high']) {
  $oblique_high = "Oblique high; "; }
else
  {$oblique_high = ""; }
if ($_REQUEST['oblique_low']) {
  $oblique_low = "Oblique low;"; }
else
  {$oblique_low = ""; }
$opd = $bw . $bw_IR . $color . $color_IR . $printt . $pos_trans . $negative . $digital . $roll . $cut_frame . $vertical . $oblique_high . $oblique_low;
chop($opd);

    $filed_by   = $_REQUEST['filed_by'];
    $official_flight_id    = $_REQUEST['official_flight_id'];
    $filed_by_in_catalog   = $_REQUEST['filed_by_in_catalog'];
    $filed_by_in_collection = $_REQUEST['filed_by_in_collection'];
//*********    $location   = $_REQUEST['location'];
    $special_location   = $_REQUEST['special_location'];
    $scale_1   = $_REQUEST['scale_1'];
    $scale_2   = $_REQUEST['scale_2'];
    $scale_3   = $_REQUEST['scale_3'];
//    $index_type   = $_REQUEST['index_type'];
    $index_scale   = $_REQUEST['index_scale'];
    $index_filed_under   = $_REQUEST['index_filed_under'];
    $size   = $_REQUEST['size'];
    $height   = $_REQUEST['height'];
    $width   = $_REQUEST['width'];
	$vertical   = $_REQUEST['vertical'];
	$oblique_high   = $_REQUEST['oblique_high'];
	$oblique_low   = $_REQUEST['oblique_low'];
	$overlap   = $_REQUEST['overlap'];
	$sidelap   = $_REQUEST['sidelap'];
	$filmtype   = $_REQUEST['filmtype'];
	$bw   = $_REQUEST['bw'];
	$bw_IR   = $_REQUEST['bw_IR'];
	$color   = $_REQUEST['color'];
	$color_IR   = $_REQUEST['color_IR'];
	$spectral_range   = $_REQUEST['spectral_range'];
	$printt   = $_REQUEST['printt'];
	$pos_trans   = $_REQUEST['pos_trans'];
	$negative   = $_REQUEST['negative'];
	$digital   = $_REQUEST['digital'];
	$roll   = $_REQUEST['roll'];
	$cut_frame   = $_REQUEST['cut_frame'];
	$filter   = $_REQUEST['filter'];
	$generation_held   = $_REQUEST['generation_held'];
	$camera   = $_REQUEST['camera'];
	$lens_focal_length   = $_REQUEST['lens_focal_length'];
	$platform_id   = $_REQUEST['platform_id'];
	$directional_orientation   = $_REQUEST['directional_orientation'];
	$altitude_a   = $_REQUEST['altitude_a'];
	$altitude_b   = $_REQUEST['altitude_b'];
    $altitude_c   = $_REQUEST['altitude_c'];
	$contractor_requestor   = $_REQUEST['contractor_requestor'];
	$flown_by   = $_REQUEST['flown_by'];
	$acquired_from   = $_REQUEST['acquired_from'];
	$note   = $_REQUEST['note'];
    $copyright   = $_REQUEST['copyright'];
	$access_limitations   = $_REQUEST['access_limitations'];
    $area_general   = $_REQUEST['area_general'];
	$counties   = $_REQUEST['counties'];
	$ready_ref = $_REQUEST['ready_ref'];
	$index_digital   = $_REQUEST['index_digital'];
	$frames_scanned   = $_REQUEST['frames_scanned'];
	$estimated_frame_count   = $_REQUEST['estimated_frame_count'];

 	$county_state_country = $_REQUEST['chkCounty'];
 	$state_country = $_REQUEST['chkState'];
 	$country = $_REQUEST['chkCountry'];


//build and issue query
$sql ="insert ap_flights set
    filed_by   = '$filed_by',
    official_flight_id    = '$official_flight_id',
    filed_by_in_catalog   = '$filed_by_in_catalog',
    filed_by_in_collection = '$filed_by_in_collection',
    location   = '$location',
    special_location   = '$special_location',
    scale_1   = '$scale_1',
    scale_2   = '$scale_2',
    scale_3   = '$scale_3',
    index_type   = '$index_type',
    index_scale   = '$index_scale',
    index_filed_under   = '$index_filed_under',
    begin_date   = '$begin_date',
    end_date   = '$end_date',
    size   = '$size',
    height   = '$height',
    width   = '$width',
    vertical   = '$vertical',
	oblique_high   = '$oblique_high',
	oblique_low   = '$oblique_low',
	overlap   = '$overlap',
	sidelap   = '$sidelap',
	filmtype   = '$filmtype',
	bw   = '$bw',
	bw_IR   = '$bw_IR',
	color   = '$color',
	color_IR   = '$color_IR',
	spectral_range   = '$spectral_range',
	printt   = '$printt',
	pos_trans   = '$pos_trans',
	negative   = '$negative',
	digital   = '$digital',
	roll   = '$roll',
	cut_frame   = '$cut_frame',
	filter   = '$filter',
	generation_held   = '$generation_held',
	camera   = '$camera',
	lens_focal_length   = '$lens_focal_length',
	platform_id   = '$platform_id',
	directional_orientation   = '$directional_orientation',
	altitude_a   = '$altitude_a',
	altitude_b   = '$altitude_b',
    altitude_c   = '$altitude_c',
	contractor_requestor   = '$contractor_requestor',
	flown_by   = '$flown_by',
	acquired_from   = '$acquired_from',
	note   = '$note',
    copyright   = '$copyright',
	access_limitations   = '$access_limitations',
    area_general   = '$area_general',
	index_digital   = '$index_digital',
	frames_scanned   = '$frames_scanned',
	counties = '$counties',
	ready_ref = '$ready_ref',
	prod_test = 'test',
	estimated_frame_count   = '$estimated_frame_count'";


$result = @mysql_query($sql,$MilWebAppsdb1mysql) or die(mysql_error());

//*  get last holding_id added to the ap_flights table

$sql2 = "SELECT holding_id FROM ap_flights ORDER BY holding_id DESC LIMIT 1";
$last_holding_id = @mysql_query($sql2,$MilWebAppsdb1mysql) or die(mysql_error());

$row_last_holding_id = mysql_fetch_array($last_holding_id);
$last_holding_id_added = $row_last_holding_id['holding_id'];

//* has there been any county information eneterd?
if (isset($_REQUEST['chkCounty']))  {
	foreach($county_state_country as $item) {
		$spot = explode("xx",$item,3);
		$county_toadd = $spot[0];
		$state_toadd = $spot[1];
		$country_toadd = $spot[2];

		$sql_string1 = "insert ap_flights_loc_county set
		holding_id  = '$last_holding_id_added',
		country_id = '$country_toadd',
		state_id = '$state_toadd',
		county_id = '$county_toadd'";
		$result = @mysql_query($sql_string1,$MilWebAppsdb1mysql) or die(mysql_error());
	 }
}

//* has there been any state information eneterd?
if (isset($_REQUEST['chkState']))  {
	foreach($state_country as $item) {
		 $spot = explode("xx",$item,2);
		 $state_toadd = $spot[0];
		 $country_toadd = $spot[1];

		 $sql_string2 = "insert ap_flights_loc_state set
		 holding_id  = '$last_holding_id_added',
		 country_id = '$country_toadd',
		 state_id = '$state_toadd'";
		 $result = @mysql_query($sql_string2,$MilWebAppsdb1mysql) or die(mysql_error());
	}
}

//* has there been any country information eneterd?
if (isset($_REQUEST['chkCountry']))  {
	foreach($country as $item) {
		 $country_toadd = $item;

		 $sql_string3 = "insert ap_flights_loc_country set
		 holding_id  = '$last_holding_id_added',
		 country_id = '$country_toadd'";
		 $result = @mysql_query($sql_string3,$MilWebAppsdb1mysql) or die(mysql_error());

	 }
}

include("index.php");
?>


<?php require_once('../Connections/MilWebAppsdb1mysql.php');
header('Content-Type: text/html; charset=utf-8');
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$implode_country = " ";
$implode_state = " ";
$implode_county = " ";

$colname_Recordset1 = "-1";
if (isset($_GET['filed_by'])) {
  $colname_Recordset1 = $_GET['filed_by'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM ap_flights WHERE filed_by = %s", GetSQLValueString($colname_Recordset1, "text"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$colname_Recordset1 = "-1";
if (isset($_GET['holding_id'])) {
  $colname_Recordset1 = $_GET['holding_id'];
}

$selected_holding = $row_Recordset1['holding_id'];

//* get all the countries for a particular holding_id
$query_holdings_country_values = sprintf("SELECT ap_flights_loc_country.country_id, country_values.country, country_values.country_sort_order FROM ap_flights_loc_country, country_values WHERE ap_flights_loc_country.holding_id = %s and ap_flights_loc_country.country_id = country_values.country_id order by country_values.country_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_country_values = mysql_query($query_holdings_country_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
$totalRows_holdings_country_values = mysql_num_rows($holdings_country_values);

//* get all the states for a particular holding_id
$query_holdings_state_values = sprintf("SELECT ap_flights_loc_state.country_id, ap_flights_loc_state.state_id, state_values.state, state_values.state_sort_order FROM ap_flights_loc_state, state_values WHERE ap_flights_loc_state.holding_id = %s and ap_flights_loc_state.state_id = state_values.state_id order by state_values.state_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_state_values = mysql_query($query_holdings_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
$totalRows_holdings_state_values = mysql_num_rows($holdings_state_values);

//**** get all the states for a particular holding_id
$query_holdings_state_values = sprintf("SELECT ap_flights_loc_state.country_id, ap_flights_loc_state.state_id, state_values.state, state_values.country_id, state_values.state_sort_order, country_values.country FROM ap_flights_loc_state, state_values, country_values WHERE ap_flights_loc_state.holding_id = %s and ap_flights_loc_state.state_id = state_values.state_id and ap_flights_loc_state.country_id = state_values.country_id and ap_flights_loc_state.country_id = country_values.country_id order by state_values.state_sort_order", GetSQLValueString($selected_holding, "int"));

$holdings_state_values = mysql_query($query_holdings_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
$totalRows_holdings_state_values = mysql_num_rows($holdings_state_values);

//**** get all the counties for a particular holding_id
 $query_holdings_county_values = sprintf("SELECT ap_flights_loc_county.country_id, ap_flights_loc_county.state_id, ap_flights_loc_county.county_id, county_values.county, state_values.state, county_values.county_sort_order FROM ap_flights_loc_county, county_values, state_values WHERE ap_flights_loc_county.holding_id = %s and ap_flights_loc_county.county_id = county_values.county_id and ap_flights_loc_county.state_id = state_values.state_id order by county_values.county_sort_order", GetSQLValueString($selected_holding, "int"));

 $holdings_county_values = mysql_query($query_holdings_county_values, $MilWebAppsdb1mysql) or die(mysql_error());
 $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
 $totalRows_holdings_county_values = mysql_num_rows($holdings_county_values);

//* get all country names associated with that holding id
//* set pointer to start of resultset array
	  $rows = mysql_num_rows($holdings_country_values);
	  if($rows > 0) {
 	      mysql_data_seek($holdings_country_values, 0);
      }

//* build string of all the country names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_country_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_country[] = $row['country']; // fetch each row...
      }

if (isset($resultset2_country)) {
	$implode_country = implode(", ",$resultset2_country);
//* 	echo "<br> yyyy ia m here country " . $implode_country . " xxxx <br>";
}


//* reset pointer to start of array for future reads
	  $rows2 = mysql_num_rows($holdings_country_values);
	  if($rows2 > 0) {
	      mysql_data_seek($holdings_country_values, 0);
	  $row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
	  }

//* get all state names associated with that holding id
//* set pointer to start of resultset array
	  $rows_state = mysql_num_rows($holdings_state_values);
	  if($rows_state > 0) {
 	      mysql_data_seek($holdings_state_values, 0);
      }

//* build string of all the state names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_state_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_state[] = $row['state']; // fetch each row...
      }

if (isset($resultset2_state)) {
	$implode_state = implode(", ",$resultset2_state);
}

//* reset pointer to start of array for future reads
	  $rows_state2 = mysql_num_rows($holdings_state_values);
	  if($rows_state2 > 0) {
	      mysql_data_seek($holdings_state_values, 0);
	  $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
	  }

//* get all county names associated with that holding id
//* set pointer to start of resultset array
	  $rows_county = mysql_num_rows($holdings_county_values);
	  if($rows_county > 0) {
 	      mysql_data_seek($holdings_county_values, 0);
      }

//* build string of all the county names that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_county_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2_county[] = $row['county']; // fetch each row...
      }

if (isset($resultset2_county)) {
	$implode_county = implode(", ",$resultset2_county);
//* echo "<br> xxxx iam here county " . $implode_county . "xxxx <br>";
}

//* reset pointer to start of array for future reads
	  $rows_county2 = mysql_num_rows($holdings_county_values);
	  if($rows_county2 > 0) {
	      mysql_data_seek($holdings_county_values, 0);
	  $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
	  }

//* get all the stored location(s) for a specific holding_id for prepopulation of reporting screen data
$colname_location_id = $row_Recordset1['location'];
$location_id = (explode(" ",$colname_location_id));

//* get all the possible values for the location of the MIL collection
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);

include("../common_code/include_physical_fields_details.php");

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

<title>Flight <?php echo $row_Recordset1['filed_by']; ?></title>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>

<?php
include("../common_code/include_MIL_header.php");
?>

<table  class="MILleft-margin-40 MILtop-margin-10" width="975" border="0" align="left" cellpadding="10" cellspacing="10">
  <tr>
    <td align="center" ><table class="MILtable" width="70%" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
        <td class="MILtd" bgcolor="#FFFFFF"><div align="center" class="MILfont-x-large">Imagery Report: Flight <?php echo $row_Recordset1['filed_by']; ?>
              <span class="MILfont-medium">
              <input type="hidden" name="hiddenField">
       	<?php if ($row_Recordset1['frames_scanned'] == 1)  {  ?>
		<span class="MILfont-red"><br>Digital</span>
		<?php ; } ?>
		<?php if ($row_Recordset1['frames_scanned'] == 2)  {  ?>
		<span class="MILfont-red"><br>Partially Digital</span>
		<?php ; } ?>

		<?php
		if ($row_Recordset1['index_digital'] == 1) {
			$flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
			$flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
			$flight_id_lower = strtolower($flight_id_cleaner);
		?>
        <br>
			<span class="MILlink"><a href="http://mil.library.ucsb.edu/ap_indexes/<?php echo $flight_id_lower; ?>/">View Index</a></span>

		<?php
		}
		if ($row_Recordset1['index_digital'] == 0)  { ?>
         <br>Index: Ask MIL Staff
		<?  } ;
		?>

		</div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="975" border="0" align="left" cellpadding="5" cellspacing="0">
      <tr valign="top">
        <td width="33%" valign="top" class="MILfont-medium">

        <table class="MILtable" width="325" align="left" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
            <tr valign="baseline">
              <td class="MILtd MILfont-bold" align="right">Country:</td>
              <td class="MILtd" width="50%" colspan="2"><?php echo $implode_country; ?> </td>
            </tr>

<!-- **** beginning of now display all the states in all the countries **** -->

  <?php
  if ($totalRows_holdings_state_values>0) { //* records found for states
  ?>

           <tr valign="baseline">
              <td class="MILtd MILfont-bold" valign="top" align="right">State(s):</td>
              <td class="MILtd"  valign="top" width="50%" colspan="2">

  <?php

  //* reset pointer to start of array for future reads

  	 $rows22 = mysql_num_rows($holdings_state_values);
       if($rows22 > 0 ) {
          mysql_data_seek($holdings_state_values, 0);
          $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
  		  }

  $current_country = $row_holdings_state_values['country'];
  $current_country_id = $row_holdings_state_values['country_id'];

  $new_country = "abc";
  $current_country = chop($current_country);
  $another_new_country = false;

//*************HERERHERHERHERHERH

  for ($jj=0; $jj<$totalRows_holdings_state_values; $jj++)
    {  //* beginning of jj LOOP statement

     if ($current_country != $new_country or $new_country == "") { // ** new country found or end of read in holdings

        if ($jj==0) {

             if ($totalRows_holdings_country_values>1) {

			 echo $row_holdings_state_values['country'];
			 echo ": <br>";
			 }
         	 ?>
			 <table frame=void margin=0 valign="top" cellspacing="0" cellpadding="0" border=0>
				<tr>
				<?php
				if ($totalRows_holdings_country_values>1) {
                 ?>
                 <td class="paddedAndFont">
                 <?php
                 }
                 else
                 {
                 ?>
                 <td class="MILfont-medium">
                 <?php
                 }

//*			 $current_country = $new_country;
			 }
			 else {
			 ?>
			</td></tr></table>
			<?php
			 echo $row_holdings_state_values['country'];
			 echo ": <br>";
        	 ?>
			 <table frame=void margin=0 valign="top" cellspacing="0" cellpadding="0" border=0>
				<tr> <td class="paddedAndFont">
<?php
            $current_country = $new_country;
			}
       }

?>
<span class="nobreak">
<?php
echo chop($row_holdings_state_values['state']);

$rows = mysql_num_rows($holdings_state_values);
if($rows > 0 ) {
	  $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
	  $new_country = $row_holdings_state_values['country'];
	  $new_country_id = $row_holdings_state_values['country_id'];
	  $new_country = chop($new_country);
	  if ($current_country == $new_country) {
	  echo ",";
	     }
	  }
?>
</span>
<?php
    } //* end of jj loop
?>

    </td></tr></table>


  </td></tr>

<?php
} //* end of records found for states
?>

<!-- **** end of now display all the states in all the countries **** -->


<!-- **** beginning of now display all the counties in all the states **** -->

 <?php
  if ($totalRows_holdings_county_values>0) { //* records found for counties
 ?>
           <tr valign="baseline">
              <td class="MILtd  MILfont-bold" align="right"  >Counties:</td>
              <td class="MILtd" width="50%" colspan="2">

  <?php
//*  if ($totalRows_holdings_county_values==0) {
//*   echo " &nbsp;";
//*  }

  //* reset pointer to start of array for future reads

  	 $rows333 = mysql_num_rows($holdings_county_values);
       if($rows333 > 0 ) {
          mysql_data_seek($holdings_county_values, 0);
          $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
  		  }

  $current_state = $row_holdings_county_values['state'];
  $current_state_id = $row_holdings_county_values['state_id'];

  $new_state = "abc";
  $current_state = chop($current_state);
  $another_new_state = false;

//************herherherherhe

  for ($jj=0; $jj<$totalRows_holdings_county_values; $jj++)
    {  //* beginning of jj LOOP statement

     if ($current_state != $new_state or $new_state == "") { // ** new state found or end of read in holdings

        if ($jj==0) {

			 echo $row_holdings_county_values['state'];
			 echo ": <br>";
         	 ?>
			 <table border="0">
				<tr> <td class="paddedAndFont">
			 <?php
			 }
			 else {
			 ?>
			</td></tr></table>
			<?php
			 echo $row_holdings_county_values['state'];
			 echo ": <br>";
        	 ?>
			 <table border="0">
				<tr> <td class="paddedAndFont">
<?php
             $current_state = $new_state;
			}
       }

?>
<span class="nobreak">
<?php
echo chop($row_holdings_county_values['county']);
?>
<?php
$rows = mysql_num_rows($holdings_county_values);
if($rows > 0 ) {
	  $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
	  $new_state = $row_holdings_county_values['state'];
	  $new_state_id = $row_holdings_county_values['state_id'];
	  $new_state = chop($new_state);
	  if ($current_state == $new_state) {
	  echo",";
	     }
	  }
?>
</span>
<?php
    } //* end of jj loop
?>

</td></tr></table>

<?php
} //* end of records found for counties
?>
  </td></tr>

<!-- **** end of now display all the counties in all the states **** -->

<!--            <tr valign="baseline">
              <td align="right" class="MILfont-medium MILfont-bold">State(s):</td>
              <td width="50%" colspan="2" class="MILfont-medium"><?php echo $implode_state; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="MILfont-medium  MILfont-bold">Counties:</td>
              <td colspan="2" class="MILfont-medium"><?php echo $implode_county; ?></td>
            </tr>
-->

            <tr valign="baseline">
              <td class="MILtd MILfont-bold" align="right">Filed by
            (catalog):</td>
              <td class="MILtd" colspan="2"><?php echo $row_Recordset1['filed_by_in_catalog']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td class="MILtd MILfont-bold" align="right">Filed by&nbsp;
              (collection):</td>
              <td class="MILtd" colspan="2"><?php echo $row_Recordset1['filed_by_in_collection']; ?> </td>
            </tr>

            <?php if (($row_Recordset1['location'] === null) or ($row_Recordset1['location'] === '' )) {
						   '  '; }
		   else {
		   ?>
	          <tr valign="baseline">
	              <td class="MILtd MILfont-bold" align="right" nowrap >Imagery Location:</td>
		   		  <td class="MILtd" colspan="2">
           <?php
include("../common_code/include_location_details.php");
               ?>
          </td>
          </tr>
          <?php  } ; ?>

            <?php if (($row_Recordset1['index_type'] == 'none') or ($row_Recordset1['index_type'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td class="MILtd MILfont-bold" align="right">Index type:</td>
              <td class="MILtd" colspan="2"><?php echo $row_Recordset1['index_type']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['index_scale'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td class="MILtd MILfont-bold" align="right">Index scale:</td>
              <td colspan="2" class="MILtd"><?php
			  if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['index_scale']))
			  {
			     // all digits so display comma delineated
			     echo "1:".number_format($row_Recordset1['index_scale']);
			  }
			  else
			  {
			     // not all digits so just show the field as it is
			     echo "1:".$row_Recordset1['index_scale'];
			  }
			  ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['index_filed_under'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Index filed under:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['index_filed_under']; ?> </td>
            </tr>
            <?  } ; ?>

             <?php
             if ($row_Recordset1['size'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Size:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['size']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['height'] > 0 )  { ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Height:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['height']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['width'] > 0 )  { ?>
			  <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Width:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['width']; ?> </td>
            </tr>
            <?  } ; ?>

        </table></td>

        <td width="33%" valign="top" class="MILfont-medium">

        <table class="MILtable" width="325" border="1" bordercolor="#c0c0c0" align="left" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">

		  <tr valign="baseline">
		   <td width="50%" class="MILtd MILfont-bold" align="right">Begin date:</td>
			 <?php
			// convert mysql date to php timestamp
			$phptimestamp = strtotime( $row_Recordset1['begin_date'] );
			// now format php timestamp
			$begin_date = date( 'Y-m-d ', $phptimestamp );
			?>
		<td class="MILtd" nowrap><div align="left"><?php echo $begin_date; ?></div> </td>
		  </tr>
		  <tr valign="baseline">
		   <td class="MILtd MILfont-bold" align="right">End date:</td>
			 <?php
			// convert mysql date to php timestamp
			$phptimestamp = strtotime( $row_Recordset1['end_date'] );
			// now format php timestamp
			$end_date = date( 'Y-m-d ', $phptimestamp );
			?>
		<td class="MILtd" nowrap><div align="left"><?php echo $end_date; ?></div> </td>
		</tr>

          <?php if ($row_Recordset1['scale_1'] > 0)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Scale:</td>
             <td class="MILtd" ><?php echo "1:".number_format($row_Recordset1['scale_1']); ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['scale_2'] > 0)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Scale:</td>
              <td class="MILtd"><?php echo "1:".number_format($row_Recordset1['scale_2']); ?>  </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['scale_3'] > 0)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Scale:</td>
              <td class="MILtd"><?php echo "1:".number_format($row_Recordset1['scale_3']); ?>  </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['overlap'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Overlap:</td>
              <td class="MILtd"><?php echo $row_Recordset1['overlap']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['sidelap'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Sidelap:</td>
              <td class="MILtd"><?php echo $row_Recordset1['sidelap']; ?> </td>
            </tr><?  } ; ?>

            <?php if ($row_Recordset1['directional_orientation'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Directional&nbsp; orientation:</td>
             <td class="MILtd"><?php echo $row_Recordset1['directional_orientation']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['platform_id'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Platform id:</td>
             <td class="MILtd"><?php echo $row_Recordset1['platform_id']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['altitude_a'] > 0)  {  ?>
	        <tr valign="baseline">
	          <td class="MILtd MILfont-bold" align="right">Altitude:</td>
              <td class="MILtd"><?php
     			if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_a']))
    			{
    			   // all digits so display comma delineated
    			   echo number_format($row_Recordset1['altitude_a']);
    			}
    			else
    			{
    			   // not all digits so just show the field as it is
    			   echo $row_Recordset1['altitude_a'];
    			}
    			?>
             </td>
		    </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['altitude_b'] > 0)  {  ?>
            <tr valign="baseline">
	          <td class="MILtd MILfont-bold" align="right">Altitude:</td>
              <td class="MILtd"><?php
     			if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_b']))
    			{
    			   // all digits so display comma delineated
    			   echo number_format($row_Recordset1['altitude_b']);
    			}
    			else
    			{
    			   // not all digits so just show the field as it is
    			   echo $row_Recordset1['altitude_b'];
    			}
    			?>
             </td>
            </tr>
             <?  } ; ?>

            <?php if ($row_Recordset1['altitude_c'] > 0)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Altitude:</td>
              <td class="MILtd"><?php
     			if (preg_match('/^([0-9]+)$/iu', $row_Recordset1['altitude_c']))
    			{
    			   // all digits so display comma delineated
    			   echo number_format($row_Recordset1['altitude_c']);
    			}
    			else
    			{
    			   // not all digits so just show the field as it is
    			   echo $row_Recordset1['altitude_c'];
    			}
    			?>
             </td>
            </tr>
             <?  } ; ?>

            <?php if ($row_Recordset1['lens_focal_length'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Lens focal length:</td>
             <td class="MILtd"><?php echo $row_Recordset1['lens_focal_length']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['camera'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Camera:</td>
             <td class="MILtd"><?php echo $row_Recordset1['camera']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['filmtype'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Film type:</td>
             <td class="MILtd"><?php echo $row_Recordset1['filmtype']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['spectral_range'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Spectral range:</td>
              <td class="MILtd"><?php echo $row_Recordset1['spectral_range']; ?> </td>
            </tr><?  } ; ?>

            <?php if ($row_Recordset1['filter'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Filter:</td>
              <td class="MILtd"><?php echo $row_Recordset1['filter']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['generation_held'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Generation held:</td>
              <td class="MILtd"><?php echo $row_Recordset1['generation_held']; ?> </td>
            </tr>
            <?  } ; ?>


        </table></td>
        <td width="33%" valign="top" class="MILfont-medium">

        <table class="MILtable" width="325" border="1" bordercolor="#c0c0c0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">

             <?php if ($row_Recordset1['note'] <> null)  {  ?>
            <tr align="left" valign="baseline">
             <td class="MILtd" colspan="2" valign="top"><div align="left"><span class="MILfont-bold">Note: </span>
                <table  border="0">
                <tr>
                 <td><?php echo $row_Recordset1['note']; ?></td>
                </tr>
                </table></div>              </td>
            </tr>
             <?  } ; ?>

            <tr valign="baseline">
             <td width="50%" class="MILtd MILfont-bold" align="right">Physical Details:</td>
             <td class="MILtd" colspan="2">
              	      <span id='span1'><?php echo $bw_describe; ?></span>
			  	      <span id='span2'><?php echo $bw_IR_describe; ?></span>
			  	      <span id='span3'><?php echo $color_describe; ?></span>
			  	      <span id='span4'><?php echo $color_IR_describe; ?></span>
			  	      <span id='span5'><?php echo $printt_describe; ?></span>
			  	      <span id='span6'><?php echo $pos_trans_describe; ?></span>
			  	      <span id='span7'><?php echo $negative_describe; ?></span>
			  	      <span id='span8'><?php echo $digital_describe; ?></span>
			  	      <span id='span9'><?php echo $roll_describe; ?></span>
			  	      <span id='span10'><?php echo $cut_frame_describe; ?></span>
			  	      <span id='span11'><?php echo $vertical_describe; ?></span>
			  	      <span id='span12'><?php echo $oblique_high_describe; ?></span>
	                  <span id='span13'><?php echo $oblique_low_describe; ?></span>
                   &nbsp;</td>
            </tr>

            <?php if (($row_Recordset1['copyright'] == 'none') or ($row_Recordset1['copyright'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Copyright:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['copyright']; ?>  </td>
            </tr>
            <?  } ; ?>
            <?php if (($row_Recordset1['access_limitations'] == 'none') or ($row_Recordset1['access_limitations'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Access limitations:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['access_limitations']; ?> </td>
            </tr>
            <?  } ; ?>

            <?php if ($row_Recordset1['flown_by'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Flown by:</td>
             <td class="MILtd"><?php echo $row_Recordset1['flown_by']; ?> </td>
            </tr>
             <?  } ; ?>

            <?php if ($row_Recordset1['contractor_requestor'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Contractor/requestor:</td>
             <td class="MILtd"><?php echo $row_Recordset1['contractor_requestor']; ?> </td>
            </tr>
             <?  } ; ?>

            <?php if ($row_Recordset1['acquired_from'] <> null)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Acquired from:</td>
             <td class="MILtd"><?php echo $row_Recordset1['acquired_from']; ?></td>
            </tr>
             <?  } ; ?>

            <?php if ($row_Recordset1['estimated_frame_count'] > 0)  {  ?>
            <tr valign="baseline">
             <td class="MILtd MILfont-bold" align="right">Est. frame count:</td>
             <td class="MILtd" colspan="2"><?php echo $row_Recordset1['estimated_frame_count']; ?> </td>
            </tr>
              <?  } ; ?>

         </table></td>
      </tr>
    </table>
   </td>
  </tr>
</table>

<?php
include("../common_code/include_MIL_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);

?>

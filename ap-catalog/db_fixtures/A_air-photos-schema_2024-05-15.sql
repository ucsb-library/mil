-- MariaDB dump 10.19  Distrib 10.8.3-MariaDB, for osx10.17 (arm64)
--
-- Host: air-photos-dev.cpjyf1fywhjq.us-west-2.rds.amazonaws.com    Database: air-photos
-- ------------------------------------------------------
-- Server version	10.6.16-MariaDB-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ap_flights`
--

DROP TABLE IF EXISTS `ap_flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_flights` (
  `holding_id` int(11) NOT NULL AUTO_INCREMENT,
  `filed_by` varchar(150) DEFAULT NULL,
  `official_flight_id` varchar(150) DEFAULT NULL,
  `filed_by_in_catalog` varchar(150) DEFAULT NULL,
  `filed_by_in_collection` varchar(150) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `special_location` varchar(100) DEFAULT NULL,
  `scale_1` int(11) DEFAULT NULL,
  `scale_2` int(11) DEFAULT NULL,
  `scale_3` int(11) DEFAULT NULL,
  `index_type` varchar(50) DEFAULT NULL,
  `index_scale` varchar(20) DEFAULT NULL,
  `index_filed_under` varchar(50) DEFAULT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `height` float DEFAULT NULL,
  `width` float DEFAULT NULL,
  `vertical` smallint(6) DEFAULT NULL,
  `oblique_high` smallint(6) DEFAULT NULL,
  `oblique_low` smallint(6) DEFAULT NULL,
  `overlap` varchar(100) DEFAULT NULL,
  `sidelap` varchar(50) DEFAULT NULL,
  `filmtype` varchar(100) DEFAULT NULL,
  `bw` smallint(6) DEFAULT NULL,
  `bw_IR` smallint(6) DEFAULT NULL,
  `color` smallint(6) DEFAULT NULL,
  `color_IR` smallint(6) DEFAULT NULL,
  `spectral_range` varchar(100) DEFAULT NULL,
  `printt` smallint(6) DEFAULT NULL,
  `pos_trans` smallint(6) DEFAULT NULL,
  `negative` smallint(6) DEFAULT NULL,
  `digital` smallint(6) DEFAULT NULL,
  `o_p_d` varchar(255) DEFAULT NULL,
  `roll` smallint(6) DEFAULT NULL,
  `cut_frame` smallint(6) DEFAULT NULL,
  `filter` varchar(100) DEFAULT NULL,
  `generation_held` varchar(100) DEFAULT NULL,
  `camera` varchar(100) DEFAULT NULL,
  `lens_focal_length` varchar(50) DEFAULT NULL,
  `platform_id` varchar(100) DEFAULT NULL,
  `directional_orientation` varchar(50) DEFAULT NULL,
  `altitude_a` varchar(50) DEFAULT NULL,
  `altitude_b` varchar(50) DEFAULT NULL,
  `altitude_c` varchar(50) DEFAULT NULL,
  `contractor_requestor` varchar(100) DEFAULT NULL,
  `flown_by` varchar(100) DEFAULT NULL,
  `acquired_from` varchar(100) DEFAULT NULL,
  `note` longtext DEFAULT NULL,
  `copyright` varchar(100) DEFAULT NULL,
  `access_limitations` varchar(50) DEFAULT NULL,
  `area_general` varchar(255) DEFAULT NULL,
  `counties` longtext DEFAULT NULL,
  `index_digital` smallint(6) DEFAULT NULL,
  `frames_scanned` smallint(6) DEFAULT NULL,
  `estimated_frame_count` int(11) DEFAULT NULL,
  `ready_ref` varchar(3) DEFAULT NULL,
  `prod_test` varchar(20) NOT NULL DEFAULT 'prod',
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `location_1` varchar(4) DEFAULT NULL,
  `location_2` varchar(4) DEFAULT NULL,
  `location_3` varchar(4) DEFAULT NULL,
  `location_4` varchar(4) DEFAULT NULL,
  `format_1` varchar(4) DEFAULT NULL,
  `format_2` varchar(4) DEFAULT NULL,
  `format_3` varchar(4) DEFAULT NULL,
  `format_4` varchar(4) DEFAULT NULL,
  `agency_code` varchar(10) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `si_status` varchar(3) DEFAULT NULL,
  `vs_status` varchar(3) DEFAULT NULL,
  `imagery_held` varchar(30) DEFAULT NULL,
  `darwin_inventory` varchar(3) DEFAULT NULL,
  `im_inventory` varchar(3) DEFAULT NULL,
  `location_change` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `collection` varchar(20) DEFAULT NULL,
  `yearDate` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`holding_id`),
  UNIQUE KEY `holding_id` (`holding_id`)
) ENGINE=MyISAM AUTO_INCREMENT=509758 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ap_flights_loc_country`
--

DROP TABLE IF EXISTS `ap_flights_loc_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_flights_loc_country` (
  `flight_loc_country_id` int(11) NOT NULL AUTO_INCREMENT,
  `holding_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`flight_loc_country_id`),
  KEY `holding_id` (`holding_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21908 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ap_flights_loc_county`
--

DROP TABLE IF EXISTS `ap_flights_loc_county`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_flights_loc_county` (
  `flight_loc_county_id` int(11) NOT NULL AUTO_INCREMENT,
  `holding_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `county_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`flight_loc_county_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37095 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ap_flights_loc_state`
--

DROP TABLE IF EXISTS `ap_flights_loc_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_flights_loc_state` (
  `flight_loc_state_id` int(11) NOT NULL AUTO_INCREMENT,
  `holding_id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`flight_loc_state_id`),
  KEY `holding_id` (`holding_id`),
  KEY `state_id` (`state_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21981 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area_general_values`
--

DROP TABLE IF EXISTS `area_general_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_general_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_general` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci PACK_KEYS=0 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country_values`
--

DROP TABLE IF EXISTS `country_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_values` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_sort_order` int(11) NOT NULL,
  `country` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `county_values`
--

DROP TABLE IF EXISTS `county_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `county_values` (
  `county_id` int(11) NOT NULL DEFAULT 0,
  `county_sort_order` int(11) NOT NULL,
  `county` varchar(50) NOT NULL,
  `state_id` int(11) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state_FIPS_code` int(11) NOT NULL,
  `county_FIPS_code` int(11) NOT NULL,
  `county_descriptor` varchar(20) NOT NULL,
  `county_map_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`county_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3142 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_values`
--

DROP TABLE IF EXISTS `location_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `region_values`
--

DROP TABLE IF EXISTS `region_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `state_values`
--

DROP TABLE IF EXISTS `state_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_values` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_sort_order` int(11) NOT NULL,
  `state` varchar(30) DEFAULT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `year_lookup`
--

DROP TABLE IF EXISTS `year_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `year_lookup` (
  `year` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci PACK_KEYS=0 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-15 18:06:08

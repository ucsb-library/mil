<?php $image_name = $_GET[image_name]; ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Frequently Requested County Map <?php echo $image_name; ?></title>

<?php
include("../common_code/include_MIL_all_style_links.php");
?>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>

<?php
include("../common_code/include_MIL_header.php");
?>

<br><br>

<a href="<?php echo $image_name; ?>"><img class="regional-breakdown-map" src="<?php echo $image_name; ?>" alt="" /></a>

<div class="MILabsolute-footer">
<?php
include("../common_code/include_MIL_footer.php");
?>
</div>



</body>
</html>

<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "-1";
if (isset($_GET['filed_by'])) {
  $colname_Recordset1 = $_GET['filed_by'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM ap_flights WHERE filed_by = %s", GetSQLValueString($colname_Recordset1, "text"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

include("include_physical_fields_details.php");

?>
<html>
<head>
<style type="text/css">
<!--
.style3 {font-size: large}
.style4 {font-size: medium}
.style7 {color: #FF0000}
.style13 {
	font-size: x-small;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>
<table width="650" border="0" align="left" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="72%"  border="2" align="center" cellpadding="5" cellspacing="5">
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style3">MIL Air Photo Flights Imagery Report <br>
              <span class="style4">Holding ID: <?php echo $row_Recordset1['holding_id']; ?>
              <input type="hidden" name="hiddenField">
        Filed By: <?php echo $row_Recordset1['filed_by']; ?></span>
		<?php if ($row_Recordset1['frames_scanned'] == 1)  {  ?>
		<span class="style7">DIGITAL</span>
		<?php ; } ?>
		</div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="650" border="1" align="left" cellpadding="0" cellspacing="1">
      <tr>
        <td width="50%" valign="top" class="style13"><table width="325" border=".5" align="left" cellpadding="5" bgcolor="#FFFFFF">
            <tr valign="baseline">
              <td align="right" class="style13" >Area-genl:</td>
              <td width="50%" colspan="2" class="style13"><?php echo $row_Recordset1['area_general']; ?> </td>
            </tr>
            <?php if ($row_Recordset1['ready_ref'] == 'yes' )  {  ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Ready ref:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['ready_ref']; ?>
              </td>
              </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['counties'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Counties:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['counties']; ?></td>
            </tr>
            <?  } ; ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Official flight id:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['official_flight_id']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="style13" >Filed by
            (catalog):</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['filed_by_in_catalog']; ?> </td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="style13" ><p>Filed by
              (collection):</p></td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['filed_by_in_collection']; ?> </td>
            </tr>
            <?php if ($row_Recordset1['special_location'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Special loc:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['special_location']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if (($row_Recordset1['index_type'] == 'none') or ($row_Recordset1['index_type'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Index type:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['index_type']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['index_scale'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Index scale:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['index_scale']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['index_filed_under'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Index filed under:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['index_filed_under']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['size'] === null )  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Size:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['size']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['height'] > 0 )  { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Height:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['height']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['width'] > 0 )  { ?>
			  <tr valign="baseline">
              <td align="right" class="style13" >Width:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['width']; ?> </td>
            </tr>
            <?  } ; ?>
            <tr valign="baseline" class="style13">
              <td colspan="3" align="right" valign="top" class="style13" >
                <div align="left"></div></td>
            </tr>
            <tr valign="baseline">
              <td align="right" class="style13" >Phys. Details:</td>
              <td colspan="2" class="style13">
              	      <span id='span1'><?php echo $bw_describe; ?></span>
			  	      <span id='span2'><?php echo $bw_IR_describe; ?></span>
			  	      <span id='span3'><?php echo $color_describe; ?></span>
			  	      <span id='span4'><?php echo $color_IR_describe; ?></span>
			  	      <span id='span5'><?php echo $printt_describe; ?></span>
			  	      <span id='span6'><?php echo $pos_trans_describe; ?></span>
			  	      <span id='span7'><?php echo $negative_describe; ?></span>
			  	      <span id='span8'><?php echo $digital_describe; ?></span>
			  	      <span id='span9'><?php echo $roll_describe; ?></span>
			  	      <span id='span10'><?php echo $cut_frame_describe; ?></span>
			  	      <span id='span11'><?php echo $vertical_describe; ?></span>
			  	      <span id='span12'><?php echo $oblique_high_describe; ?></span>
	                  <span id='span13'><?php echo $oblique_low_describe; ?></span>
                   &nbsp;</td>
            </tr>

            <?php if (($row_Recordset1['copyright'] == 'none') or ($row_Recordset1['copyright'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Copyright:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['copyright']; ?>  </td>
            </tr>
            <?  } ; ?>
            <?php if (($row_Recordset1['access_limitations'] == 'none') or ($row_Recordset1['access_limitations'] === null))  {
			   '  '; }
			   else { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Access limitations:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['access_limitations']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['index_digital'] == 1)  { ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Index digital:</td>
              <td class="style13">Yes</td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['frames_scanned'] == 1)  {  ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Frames scanned:</td>
              <td class="style13">Yes</td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['estimated_frame_count'] > 0)  {  ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Est. frame count:</td>
              <td colspan="2" class="style13"><?php echo $row_Recordset1['estimated_frame_count']; ?> </td>
            </tr>
              <?  } ; ?>
            <tr align="left" valign="baseline">
              <td align="right" valign="baseline" class="style13" ><p>Prod/Test:</td>
              <td valign="top" class="style13" ><?php echo $row_Recordset1['prod_test']; ?></td>
            </tr>
        </table></td>
        <td width="50%" valign="top" class="style13"><table width="325" border=".5" cellpadding="5" bgcolor="#FFFFFF">
            <tr valign="baseline">
              <td align="right" class="style13" >Begin date:</td>
               <?php
				// convert mysql date to php timestamp
				$phptimestamp = strtotime( $row_Recordset1['begin_date'] );
				// now format php timestamp
				$begin_date = date( 'Y-m-d ', $phptimestamp );
				?>
    		 <td nowrap><div align="left" class="style13"><?php echo $begin_date; ?></div> </td>
            </tr>
            <tr valign="baseline">
              <td  align="right" class="style13">End date:</td>
               <?php
				// convert mysql date to php timestamp
				$phptimestamp = strtotime( $row_Recordset1['end_date'] );
				// now format php timestamp
				$end_date = date( 'Y-m-d ', $phptimestamp );
				?>
    		 <td nowrap><div align="left" class="style13"><?php echo $end_date; ?></div> </td>
            </tr>
            <?php if ($row_Recordset1['scale_1'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Scale 1:</td>
              <td class="style13"><?php echo $row_Recordset1['scale_1']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['scale_2'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Scale 2:</td>
              <td class="style13"><?php echo $row_Recordset1['scale_2']; ?>  </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['scale_3'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Scale 3:</td>
              <td class="style13"><?php echo $row_Recordset1['scale_3']; ?>  </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['overlap'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Overlap:</td>
              <td class="style13"><?php echo $row_Recordset1['overlap']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['sidelap'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Sidelap:</td>
              <td class="style13"><?php echo $row_Recordset1['sidelap']; ?> </td>
            </tr><?  } ; ?>
            <?php if ($row_Recordset1['spectral_range'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Spectral range:</td>
              <td class="style13"><?php echo $row_Recordset1['spectral_range']; ?> </td>
            </tr><?  } ; ?>
            <?php if ($row_Recordset1['filter'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Filter:</td>
              <td class="style13"><?php echo $row_Recordset1['filter']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['generation_held'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Generation held:</td>
              <td class="style13"><?php echo $row_Recordset1['generation_held']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['filmtype'] <> null)  {  ?>
            <tr valign="baseline">
              <td align="right" class="style13" >Film type:</td>
              <td class="style13"><?php echo $row_Recordset1['filmtype']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['camera'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Camera:</td>
              <td class="style13"><?php echo $row_Recordset1['camera']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['lens_focal_length'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Lens focal length:</td>
              <td class="style13"><?php echo $row_Recordset1['lens_focal_length']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['platform_id'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Platform id:</td>
              <td class="style13"><?php echo $row_Recordset1['platform_id']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['directional_orientation'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Directional orientation:</td>
              <td class="style13"><?php echo $row_Recordset1['directional_orientation']; ?> </td>
            </tr>
            <?  } ; ?>
            <?php if ($row_Recordset1['altitude_a'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Altitude a:</td>
              <td class="style13"><?php echo $row_Recordset1['altitude_a']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['altitude_b'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Altitude b:</td>
              <td class="style13"><?php echo $row_Recordset1['altitude_b']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['altitude_c'] > 0)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Altitude c:</td>
              <td class="style13"><?php echo $row_Recordset1['altitude_c']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['contractor_requestor'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Contractor/requestor:</td>
              <td class="style13"><?php echo $row_Recordset1['contractor_requestor']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['flown_by'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Flown by:</td>
              <td class="style13"><?php echo $row_Recordset1['flown_by']; ?> </td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['acquired_from'] <> null)  {  ?>
            <tr valign="baseline">
              <td  align="right" class="style13">Acquired from:</td>
              <td class="style13"><?php echo $row_Recordset1['acquired_from']; ?></td>
            </tr>
             <?  } ; ?>
            <?php if ($row_Recordset1['note'] <> null)  {  ?>
            <tr align="left" valign="baseline">
              <td colspan="2" valign="top" class="style13" >&nbsp;</td>
            </tr>
            <tr align="left" valign="baseline">
              <td colspan="2" valign="top" class="style13" ><div align="left">Note:
                <table  border="0">
                <tr class="style13">
                  <td class="style13"><?php echo $row_Recordset1['note']; ?></td>
                </tr>
                </table></div>              </td>
            </tr>
             <?  } ; ?>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</html>
<?php
mysql_free_result($Recordset1);

?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
   <title>Northern California Frequently Requested Flights</title>
   <meta name="keywords" content="Kern, Los Angeles, Ventura, Santa Barbara, San Luis Obispo, Bakersfield, San Diego, Riverside, San Bernardino, Orange">
   <meta name="description" content="These ready reference aids contain a sub-set of our flight holdings covering areas that are frequently requested by patrons - Kern, Los Angeles, Ventura, Santa Barbara, San Luis Obispo, Bakersfield, San Diego, Riverside, San Bernardino and Orange Counties. We have tried to select the best flights over a range of years for each area listed.">

<?php
include("../common_code/include_MIL_all_style_links.php");
?>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>
<body text="#000000" bgcolor="#FFFFFF" >

<?php
include("../common_code/include_MIL_header.php");
?>

<!--
<center><b><font face="Arial, Helvetica, sans-serif"><font size=+1>Ready Reference Aids for Air Photos</font></b></center>
<p> These ready reference aids contain a <b>sub-set</b> of our flight holdings covering areas that are frequently requested by patrons. We have tried to select the best flights over a range of years for each area listed.
-->

<div class="MILleft-margin-40 MILtop-margin-10 MILfont-x-large-bold">
<br>
Frequently Requested Flights of Northern California
</div>

<table class="MILleft-margin-40 MILtop-margin-10" ><tr>

<td>
<img src="No_CaL_ready_ref_counties.jpg" width="484" height="535" border="0" usemap="#NoCalCounties" />
  <map name="NoCalCounties" id="NoCalCounties">
    <area shape="poly" coords="187,233,209,211,202,182,186,148,166,104,134,64,82,61,37,69,6,79,7,95,79,186,114,182,172,211" href="county.php?County=SONOMA"  alt="Sonoma" />
    <area shape="poly" coords="185,238,193,263,199,294,185,302,149,281,140,285,115,254,89,262,64,193,109,187,163,213" href="county.php?County=MARIN" " alt="Marin" />
    <area shape="poly" coords="199,231,222,210,236,211,233,189,249,139,328,129,329,192,345,196,321,230,292,246,222,247" href="county.php?County=SOLANO" alt="Solano" />
    <area shape="poly" coords="300,249,335,233,349,198,348,186,349,131,333,111,333,65,444,61,438,99,448,146,451,195,368,205,357,226,343,242,322,254" href="county.php?County=SACRAMENTO" alt="Sacramento" />
    <area shape="poly" coords="198,261,204,284,228,281,241,304,267,306,273,315,279,325,350,305,350,247,326,262,307,256,295,253,221,252" href="county.php?County=CONTRA COSTA" alt="Contra Costa" />
    <area shape="poly" coords="207,326,208,300,186,305,175,306,150,291,135,291,91,291,81,341,167,338,178,326" href="county.php?County=SAN FRANCISCO" alt="San Francisco" />
    <area shape="poly" coords="181,334,206,332,249,376,235,387,232,410,243,439,227,440,217,467,193,466,181,412,91,413,92,353,175,351" href="county.php?County=SAN MATEO" alt="San Mateo" />
    <area shape="poly" coords="213,291,210,330,248,368,258,369,259,380,365,380,365,371,345,360,350,315,279,333" href="county.php?County=ALAMEDA" alt="Alameda" />
    <area shape="poly" coords="254,386,242,397,238,413,254,437,271,462,347,519,363,502,417,503,421,447,379,437,378,425,375,386" href="county.php?County=SANTA CLARA" alt="Santa Clara" />
    <area shape="poly" coords="355,355,358,243,375,210,452,203,466,250,470,326,422,336,379,374" href="county.php?County=SAN JOAQUIN" alt="San Joaquin" />
  </map>
</td>
</tr></table>

<!--
<p><b><font size=+1><font color="#FF0000">CAUTION!!!</font></font> Any particular flight may not cover the complete extent of the county Sub-Region it is listed under. Please refer to the online indexes, or contact us for assistance if you have any questions. Also note that the ready reference aids displayed here are not our complete holdings. We are still in the process of converting our hardcopy records and holdings into digital formats.</b>
<br>&nbsp;
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in">The Digital column denotes whether the flight has been scanned and is available in digital format.</li>
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in">Within each Sub-Region (see Sub-Region map if applicable), flights are arranged in chronological order beginning with the earliest years available.</li>
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in">Only flights with scales at 1:50,000 or greater detail are included in these ready reference aids. These ready reference aids do not represent our complete holdings.</li>
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in"><b>[P]</b> Appears below the Format column when at least a portion of the flight is in paper-print format.</li>
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in"><b>"Partial"</b> in the notes field means the flight does not cover the entire region.</li>
<li class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l2 level1 lfo3;tab-stops:list .5in"><b>[No Reproduction]</b> under the <b>Restrictions</b> column means that the Map & Imagery Laboratory (MIL) is unable to provide reproductions on these flights due to copyright restrictions.  We can provide you information to contact the owner of these flights to request reproductions directly from them.</li>
<br>&nbsp;
<hr WIDTH="50%"></hr>
</hr>
<br>
<center><b>Access to scanned indexes.</b>
-->

<div class="MILleft-margin-40 MILtop-margin-10 MILlink">
<a href="/apcatalog/ready_ref/southernCalifornia.php" onMouseOver="window.status='Frequently Requested Flights of Southern California'; return true">Frequently Requested Flights of Southern California</a>
<br>
&nbsp;
<br>
<a href="http://mil.library.ucsb.edu/ap_indexes/" onMouseOver="window.status='Aerial Photography Indexes by Flight ID'; return true">Aerial Photography Indexes listed by Flight ID</a>
<br>
&nbsp;
<br>
<a href="http://www.library.ucsb.edu/map-imagery-lab/california-aerial-photography-county" onMouseOver="window.status='Air Photo Flights listed by County'; return true">Aerial Photography Flights listed by County including access to indexes and information about the flights</a>
<br><br>
</div>

<?php
include("../common_code/include_MIL_footer.php");
?>



</body>
</html>

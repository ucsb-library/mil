<?php

//* code for the footer of all MIL pages to match the current lemon Drupil site
//* M Rankin - March 2012

?>

<!-- footer to mimic the www.library website footer -->

<div id="MILfooter2">
<div id="MILabsolute-footer">
<div id="footer" class="container-12 clear-block">
   <div id="supernav"></div>
      <div id="MILfooter" class="grid-12">
         <div id="block-menu-menu-navigation-footer-primary" class="block block-menu grid-7">
            <div class="content">
			   <ul class="menu">
				  <li class="leaf first"><a href="http://www.library.ucsb.edu/mil" title="MIL Home">MIL Home</a></li>
			      <li class="leaf last"><a href="http://www.library.ucsb.edu" title="Library Home">Library Home</a></li>
<!--
		          <li class="leaf"><a href="http://www.library.ucsb.edu/logon" title="Log into the proxy server, your library account, or your &quot;My ILL&quot; account">Footer Link 1</a></li>
		          <li class="leaf last"><a href="http://www.library.ucsb.edu/sitemap" title="">Footer Link 2</a></li>
 -->
		       </ul>
		    </div>
         </div>

         <div id="block-block-15" class="block block-block grid-5">
            <div class="content">
               <p><a class="social fb MILleft-margin-12" href="http://www.facebook.com/pages/UCSB-Library/18618265700" title="Facebook"></a> <a class="social twitter" title="Twitter" href="http://twitter.com/UCSBLibrary"></a> <a class="social flickr" title="Flickr" href="http://www.flickr.com/photos/ucsblibraries/"></a> <a title="RSS Feeds" class="social rss" href="http://www.library.ucsb.edu/news/feed"></a></p>
            </div>
         </div>

         <div id="block-menu-menu-navigation-footer-secondary" class="block block-menu grid-7">
            <div class="content">
               <ul class="menu"><li class="leaf first"><a href="http://www.library.ucsb.edu/policies" title="">Policies</a></li>
                  <li class="leaf"><a href="http://www.library.ucsb.edu/terms-use" title="Terms of use for the library website and policies for using library computer and resources">Terms of Use</a></li>
                  <li class="leaf last"><a href="http://www.library.ucsb.edu/mailing-address" title="">Mailing Address</a></li>
               </ul>
            </div>
         </div>

         <div id="block-block-16" class="block block-block grid-5">
            <div class="content">
               <ul>
		          <li class="MILlist-no-disc"><a href="http://www.library.ucsb.edu/davidson-library">Davidson Library</a> (805) 893-2478</li>
				  <li class="MILlist-no-disc"><a href="http://www.library.ucsb.edu/arts-library">Arts Library</a>  (805) 893-2850</li>
				  <li class="MILlist-no-disc">
				     <div class="icon ucsb"  style="border: 0px solid red;">
					    <div class="icon-holder">
					       <a href="http://www.ucsb.edu/"><img  src="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/images/ucsb_logi.png" /></a>
					    </div>
                        <a href="http://www.ucsb.edu/">UC Santa Barbara</a><br />
		                   Santa Barbara, CA 93106-9010
	                 </div>
                  </li>
               </ul>
            </div>
         </div>

         <div id="block-block-17" class="block block-block grid-7">
            <div class="content">
               <p>Copyright &copy; 2012 The Regents of the University of California, All Rights Reserved.</p>
            </div>
         </div>

         <div id="block-menu-menu-navigation-footer-tertiary" class="block block-menu grid-7">
            <div class="content">
		    </div>
         </div>

      </div> <!-- end of "MILfooter" class="grid-12" -->
   </div> <!-- end of supernav -->
</div> <!-- end of footer -->
</div> <!-- end of MILabsolute-footer -->
</div>  <!-- end of MILfooter2 -->

<!-- footer to mimic the www.library website footer -->
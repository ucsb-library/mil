<!-- code to customize the title bar of the different AP Indexes Flight Reports -->

<?php
        if ($report_type == 'AllCa') {
?>
        <title>All California Flights</title>
<?php
	}
	else if ($report_type == 'AllNonCa') {
?>
        <title>All other (non-California) Flights</title>
<?php
	}
        else if ($report_type == 'AllUS') {
?>
        <title>All U.S. Flights</title>
<?php
	}
	else if ($report_type == 'AllNonUS') {
?>
        <title>Foreign Flights </title>
<?php
	}
	else if ($report_type == 'AllFlights') {

        if ($report_frames_scanned_type == '1') {
		  ?>
          <title>All Digital Flights</title>
          <?php
          }
          else if ($report_frames_scanned_type =='2') {
          ?>
          <title>All Partially Digital Flights</title>
          <?php
          }
          else if ($report_frames_scanned_type =='0') {
          ?>
          <title>Analog Only Flights</title>
          <?php
          }
          else if ($report_frames_scanned_type =='99') {
          ?>
          <title>All Cataloged Flights</title>
	<?php
          }
	}
	else {
	?>
        <title>All Flights for <?php echo $row_Recordset1['county']; ?></title>
	<?php
	}

?>


<?php

//compose display variables for physical details field

  $bw_describe = '';
  $bw_IR_describe = '';
  $color_describe = '';
  $color_IR_describe = '';
  $printt_describe = '';
  $pos_trans_describe = '';
  $negative_describe = '';
  $digital_describe = '';
  $roll_describe = '';
  $cut_frame_describe = '';
  $vertical_describe = '';
  $oblique_high_describe = '';
  $oblique_low_describe = '';

  $bw_choice = $row_Recordset1['bw'];
  if ($bw_choice == 1) {$bw_describe = 'black and white; ';}
  $bw_IR_choice = $row_Recordset1['bw_IR'];
  if ($bw_IR_choice == 1) {$bw_IR_describe = 'black and white infrared; ';}
  $color_choice = $row_Recordset1['color'];
  if ($color_choice == 1) {$color_describe = 'color; ';}
  $color_IR_choice = $row_Recordset1['color_IR'];
  if ($color_IR_choice == 1) {$color_IR_describe = 'color infrared; ';}
  $printt_choice = $row_Recordset1['printt'];
  if ($printt_choice == 1) {$printt_describe = 'paper prints; ';}
  $pos_trans_choice = $row_Recordset1['pos_trans'];
  if ($pos_trans_choice == 1) {$pos_trans_describe = 'positive transparencies; ';}
  $negative_choice = $row_Recordset1['negative'];
  if ($negative_choice == 1) {$negative_describe = 'negative transparencies; ';}
  $digital_choice = $row_Recordset1['digital'];
  if ($digital_choice == 1) {$digital_describe = 'digital copy; ';}
  $roll_choice = $row_Recordset1['roll'];
  if ($roll_choice == 1) {$roll_describe = 'film roll; ';}
  $cut_frame_choice = $row_Recordset1['cut_frame'];
  if ($cut_frame_choice == 1) {$cut_frame_describe = 'cut frame; ';}
  $vertical_choice = $row_Recordset1['vertical'];
  if ($vertical_choice == 1) {$vertical_describe = 'vertical view; ';}
  $oblique_high_choice = $row_Recordset1['oblique_high'];
  if ($oblique_high_choice == 1) {$oblique_high_describe = 'oblique high; ';}
  $oblique_low_choice = $row_Recordset1['oblique_low'];
  if ($oblique_low_choice == 1) {$oblique_low_describe = 'oblique low view; ';}

// end of compose display variables for physical details field

?>
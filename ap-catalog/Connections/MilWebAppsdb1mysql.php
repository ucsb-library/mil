<?php
//         This is the MySQL DB server
// ===============================================================

$database_MilWebAppsdb1mysql = getenv("MYSQL_DATABASE");
$hostname_MilWebAppsdb1mysql = getenv("MYSQL_HOST");
$password_MilWebAppsdb1mysql = getenv("MYSQL_PASSWORD");
$username_MilWebAppsdb1mysql = getenv("MYSQL_USER");

// This is Database connection used throughout the code
// ========================================================

$MilWebAppsdb1mysql = mysql_connect($hostname_MilWebAppsdb1mysql,
                                    $username_MilWebAppsdb1mysql,
                                    $password_MilWebAppsdb1mysql)
                    or trigger_error(mysql_error(),E_USER_ERROR);

//set the default timezone for use by all php functions
date_default_timezone_set('America/Los_Angeles');
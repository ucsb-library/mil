# How to...

### Update data backing the Air Photos Catalog web application with fresh data from the Amazon hosted RDS database.

### Prerequisites:

- mysqldump software ( must be version compatible with old MySQL v5.7 )
- login credentials for AMZ RDS database
- login credentials for old MySQL database.  
    - credentials are available in Secret Server
- shell access to the old MySQL server hosted on Library systems

### Steps:

##### 1.) Make a backup of the old data

- Use `mysqldump` to save the data to a text file of SQL commands.
- you may need to adjust the path in the `--result_file` command below

#### Note: 

shell code below assumes use of 1Password and 1Password CLI (`op`) to retrieve secrets from 1Password credentials manager.  
If you're not using 1Password adjust the code below to suit your needs.

```sh
mysqldump \
 -h $(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/host)  \
 -u $(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/username) 
 -p$(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/password)  \
 --skip-opt \
 --order-by-primary \
 --disable-keys \
 --add-drop-table \
 --create-options \
 --result-file=/opt/db_dump/db-1-mysql_airphotos2_$(date +%Y-%m-%d-%H_%M).sql \
 --databases airphotos2 \
 --tables ap_flights ap_flights_loc_country ap_flights_loc_county ap_flights_loc_state country_values county_values location_values region_values state_values year_lookup
```

##### 2.) Get fresh data from AMZ RDS Database


- Use `mysqldump` to save the data to a text file of SQL commands.
- specify your database username in the `-u` command below
- you may need to adjust the path in the `--result_file` command below
- you should be prompted for your password.


```sh
mysqldump \
 -h $(op read op://personal/efn2ybf3ebedvhivd32a35f2tu/server) \
 -u $(op read op://personal/efn2ybf3ebedvhivd32a35f2tu/username) \
 -p$(op read op://personal/efn2ybf3ebedvhivd32a35f2tu/password) \
 --skip-opt \
 --order-by-primary \
 --disable-keys \
 --add-drop-table  \
 --create-options \
 --result-file=/opt/db_dump/amz_rds-air-photos_$(date +%Y-%m-%d-%H_%M).sql \
 --databases air-photos \
 --tables ap_flights ap_flights_loc_country ap_flights_loc_county ap_flights_loc_state country_values county_values location_values region_values state_values year_lookup
```
##### 3.) Transform the SQL to be compatible with the old MySQL server

the two servers have no common character set availalbe so change the specified char set in the SQL dump file like this:

be sure to change the filename to match the one you created above.

```sh
sed -i -e 's/utf8mb3/utf8/g' {{filename-from-step-2-export-of-AMZ-RDS-data}}
```


##### 4.) Move the fresh data SQL file to the MySQL server

- use `scp` to move the SQL file.
- substitue your filename for the example one in the command below.

```sh
scp {{filename-from-step-2-export-of-AMZ-RDS-data}} yourUserName@$(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/host)/{{filename-from-step-2-export-of-AMZ-RDS-data}} 
```

##### 5.) run the data from the AMZ RDS SQL file into the airphotos2 database on the old MySQL server.

- create a time stamped log file
- use `mysql` command line client
- substitute your username in the example below.
- substitute your SQL file name in the example below.
- you should be prompted for your MySQL password.

```sh
logfile=$(date "+%Y-%m-%d_%H-%M")_log.txt
touch $logfile
echo "start-time: "$(date "+%Y-%m-%d_%H-%M-%S") >> $logfile
echo "" >> $logfile
mysql -h $(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/host)  -u $(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/username) -p$(op read op://personal/lgsqtnuybjhwjokrtnilmxn5ny/password) --database airphotos2 < {{filename-from-step-2-export-of-AMZ-RDS-data}}
echo "end-time: "$(date "+%Y-%m-%d_%H-%M-%S") >> $logfile
echo "" >> $logfile
```

---

##### Note:

If you're going to use a Docker container to interact with the old MySQL database you'll need to run Docker on a computer with `X86` architecture.  Old mysql [docker image](https://hub.docker.com/_/mysql) `image: mysql:5.7` doesn't work on an Apple Silicon Mac.

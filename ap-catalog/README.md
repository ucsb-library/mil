ap-catalog
==========

this app serves out `[mil.library.ucsb.edu/apcatalog`][apcatalog]

## deveopment

from this project directory, do:

```sh
docker compose up --build
```

[apcatalog][https://mil.library.ucsb.edu/apcatalog]

//* script to generate the persistent headers on html pages where display
//* is in tabular form

//* script to generate the persistent headers on html pages where display
//* is in tabular form

function UpdateTableHeaders() {
   $(".persist-area").each(function() {

       var el             = $(this),
           offset         = el.offset(),
           scrollTop      = $(window).scrollTop(),
           floatingHeader = $(".floatingHeader", this)

       if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
           floatingHeader.css({
            "visibility": "visible"
           });
       } else {
           floatingHeader.css({
            "visibility": "hidden"
           });
       };
   });
}

// DOM Ready
$(function() {

   var clonedHeaderRow;

   $(".persist-area").each(function() {
       clonedHeaderRow = $(".persist-header", this);
       clonedHeaderRow.before(clonedHeaderRow.clone());



             clonedHeaderRow.children().css("width", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("width", val);
            });

browsername=navigator.appName;

if (browsername.indexOf("Microsoft")!=-1) {

            clonedHeaderRow.children().css("width", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("width", val);

        });

            clonedHeaderRow.children().css("border", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("border", '1px solid black');

        });

            clonedHeaderRow.children().css("padding-left", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("padding-left", '5px');

        });

            clonedHeaderRow.children().css("padding-right", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("padding-right", '3px');

        });

            clonedHeaderRow.children().css("spacing", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("spacing", '0px');

        });

            clonedHeaderRow.children().css("margin", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("margin", '0px');

        });
        
            clonedHeaderRow.children().css("margin-top", function(i, val){
            return $(clonedHeaderRow).children().eq(i).css("margin-top", '0px');

        });        

        }


       clonedHeaderRow.addClass("floatingHeader");
   });

   $(window).scroll(UpdateTableHeaders).trigger("scroll");

});

//* script function to check the user data input and to check it for the appropriate data
//* type that is suitable for that field
//* if not suitable, the user is immediately alerted as to what the accepted data types are for that field,
//* the input box for that data field is cleaned out and the
//* cursor is left in the input box ready for the user to put in the correct data type.

//* script to generate the persistent headers on html pages where display
//* is in tabular form

//* function to alert user in inappropriate type of NUMBER/CHARACTER/DECIMAL data has been entered

//* arguments element_name = the unique identifier (id tag) for that field on the page
//* arguments element_value = the input value for that unique field on the page
//* arguments element_type = the data type acceptable for that unique field on the page

function checkDataInput(element_name, element_value, element_type) {

//      alert("have reached the check numbers place element name is " + element_name + "xxxxx");
//      alert("have reached the check numbers place element value is " + element_value  + "yyyyy");
//      alert("have reached the check numbers place element type is " + element_type  + "zzzzzz");


      if (element_type == "num") {
          var num_regex = /^\d+$/; // numeric digits only
      } else if (element_type == "numdot") {
          var num_regex = /^(\d+)?(\.)?\d+$/; // numeric digits with decimal point
      } else if (element_type == "numhyphen") {
          var num_regex = /^(\d+)?(\-)?\d+$/; // numeric digits with hyphen separating the values (ie: a range)          
      } else if (element_type == "numspace") {
          var num_regex = /^[\d\s]+$/; // numeric digits with spaces between number(s)
      } else if (element_type == "numcommaspace") {
          var num_regex = /^[\d\,\s]+$/; // numeric digits with comma(s) and/or space(s)           
      } else if (element_type == "numcharhyphen") {
          var num_regex = /^[a-zA-Z0\d+\-]+$/; // number and/or character and/or hyphens
      } else {
          var num_regex = /^\d+$/; // numeric digits only
      }

      if (element_value) {  //* start of if any number was entered at all

      if  (element_value.match(num_regex))  {
//     // all digits so everything is okay
//     alert("Digits are all numbers, well done!  ");
    }
    else
    {
//     // not all digits so just show the field as it is
//          clean out the information entered and put the cursor back into the input box

      if (element_type == "num") {
     alert("Only numbers are permitted in this field. Please correct ");
      } else if (element_type == "numdot") {
     alert("Only numbers and a decimal point are permitted in this field. Please correct ");
      } else if (element_type == "numhyphen") {
     alert("Only a number or a range of numbers separated by an hyphen are permitted in this field. Please correct ");
      } else if (element_type == "numspace") {
     alert("Only numbers separated by spaces are permitted in this field. Please correct ");     
      } else if (element_type == "numcommaspace") {
     alert("Only numbers, commas and spaces are permitted in this field. Please correct ");     
      } else if (element_type == "numcharhyphen") {
     alert("Only letters, numbers and hyphens are permitted in this field. Please correct ");
      } else {
     alert("Don't know what the heck happened. Please correct ");
    }

         document.getElementById(element_name).innerHTML = "";
         document.getElementById(element_name).value = "";

         if (navigator.appName != 'Microsoft Internet Explorer')
     {
       document.getElementById(element_name).innerHTML = "";
       document.getElementById(element_name).value = "";

//* there is a problem with the way that focus is returned to the cleared out input box.
//* Netscape requires this trick timeout ... who would have thunk that IE actually behaves itself!!!!
           setTimeout(
        function()
        {
          var mytext = document.getElementById(element_name);
          mytext.focus();
          mytext.select();
        }
           , 1); //* end of setTimeout

       }
      else
       {
    //      alert("navigator name is " + navigator.appName);
            var mytext = document.getElementById(element_name);
          mytext.focus();

       } //* end of navigator.appName section

    } //* end of if (element_value.match(num_regex))

    }  //* end of if any number was entered at all

}  //* end of function checkDataInput(element_name, element_value, element_type)


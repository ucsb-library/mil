/*$Id: image_caption.js,v 1.2.2.3 2010/02/03 07:50:25 davidwhthomas Exp $*/
/* Made changes to this file -- Mai Irie 8/25/2011 */
$(document).ready(function(){
  $("#main img[title]").each(function(i) { // Look for images in the main content area
    var imgwidth = $(this).width();
    var imgheight = $(this).height();
    var captiontext = $(this).attr('title');

    var alignment = $(this).attr('class'); // Need to align based on the class given by imagecache preset
    $(this).removeClass(alignment); // Move this class to the parent div (image-caption-container)

    //Clear image styles to prevent conflicts with parent div
    $(this).attr({align:""});
    $(this).attr({style:""});
    // No longer need to include alignment or style because using styles provided by imagecache preset class
    $(this).wrap("<span class=\"image-caption-container\"></span>"); 
    $(this).parent().addClass(alignment); 
    if(imgwidth != 'undefined' && imgwidth != 0){
      $(this).parent().width(imgwidth);
    }
    $(this).parent().append("<span style=\"display:block;\" class=\"image-caption\">" + captiontext + "</span>");
  });
});
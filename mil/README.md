mil.library.ucsb.edu
====================

This project serves out the [mil][mil] root as a static site.

See the top-level [README.md][top-readme] for information about how
the rest of the site is structured.


[mil]: mil.library.ucsb.edu
[top-readme]: ../README.md

<div class="footer">
    <div>
        <ul>
            <li><a href="http://www.library.ucsb.edu/mil" title="MIL Home">MIL Home</a></li>
            <li><a href="http://www.library.ucsb.edu" title="Library Home">Library Home</a></li>
            <li><a href="http://www.ucsb.edu/">UCSB Home</a></li>
        </ul>

        <ul class="social menu">
            <li><a class="social fb" href="http://www.facebook.com/pages/UCSB-Library/156460897714927" title="Facebook"><span class="hidden">Facebook</span></a></li>
            <li><a class="social twitter" title="Twitter" href="http://twitter.com/UCSBLibrary"><span class="hidden">Twitter</span></a></li>
            <li><a class="social flickr" title="Flickr" href="http://www.flickr.com/photos/ucsblibraries/"><span class="hidden">Flickr</span></a></li>
            <li><a title="RSS Feeds" class="social rss" href="http://www.library.ucsb.edu/news/feed"><span class="hidden">RSS</span></a></li>
        </ul>
    </div>

    <div class="meta">
        <ul>
            <li><a href="http://www.library.ucsb.edu/policies">Policies</a></li>
            <li><a href="http://www.library.ucsb.edu/terms-use" title="Terms of use for the library website and policies for using library computer and resources">Terms of Use</a></li>
            <li><a href="http://www.library.ucsb.edu/accessibility" title="Accessibility">Accessibility</a></li>
            <li><a href="http://www.library.ucsb.edu/form/website-problem">Contact Library Web Manager</a></li>
        </ul>
    </div>

    <div class="contact">
        <p><a href="http://www.ucsb.edu/">UC Santa Barbara</a><br />
            Santa Barbara, CA 93106-9010
        </p>

        <ul>
            <li><a href="http://www.library.ucsb.edu/mailing-address">Mailing Address</a></li>
            <li><a href="http://www.library.ucsb.edu/davidson-library">Davidson Library</a> (805) 893-2478</li>
            <li><a href="http://www.library.ucsb.edu/arts-library">Arts Library</a>  (805) 893-2850</li>
        </ul>
    <p class="copyright">Copyright &copy; 2015 The Regents of the University of California, All Rights Reserved.</p>
    </div>
</div>

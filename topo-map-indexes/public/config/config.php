<?php
// please see documentation in the Confluence Wiki:
// https://wiki.library.ucsb.edu/display/MIL/Topo+Index+Web+App

// the root of the application.  If the application is in a sub-directory of the web
// server DocumentRoot the subdirectory name should be the $approot.  Should include leading slash but NOT trailing slash.
$approot = "/";

// where the topo index scan pdfs and data files are mounted in to.  The mount point for the scans and data files.

$data_directory = "/var/www/html/topo-index-scans"; 
$topo_index_scans_s3 = "https://topo-map-indexes.s3.us-west-2.amazonaws.com/";

$root = $_SERVER['DOCUMENT_ROOT'].$approot ;

// This data file must have unix <lf> or windows <cr><lf> line endings
$broad_divisions_datafile = $data_directory . "/broad_divisions.csv";
          $topos_datafile = $data_directory . "/foreign_topo_index_scanning.csv";

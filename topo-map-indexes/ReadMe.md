# How to update SQLite database `pdf_files` table.


The `pdf_files` table should have one row for each PDF of a scanned topographic map index available in the file system.

Anytime a scanned topographic map index is added or removed from the file system the database table should be updated by using this procedure.  The scanned topographic map index PDFs are currently (2022-07-06) stored on the NetApp storage system under the MIL directory.  The spatial data curator should have access to these files.

This procedure assumes you have a terminal/shell open and your current working directory is the same as the data file: `topo-index-scans_sqlite.db`.

It is also assumed you have your PDF files in a directory named: `topo-index-scans`.  You may need to adjust the code below to match your directory structure.

## steps to creating a new SQL file to populate the table:

### create head of SQL file:

```sh
echo "DELETE from pdf_files;\n UPDATE sqlite_sequence SET seq = 0 WHERE 'name' = 'pdf_files';\n INSERT into pdf_files (name) VALUES " > file-listing.sql
```

### list the files, add quotes, parens and commans and direct output to a SQL text file: 

```sh
ls -1 topo-index-scans/*.pdf | cut -d '/' -f2 | sed -e 's/^/("/g' -e 's/$/"),/g' -e '$ s/.$/;/' >> file-listing.sql
```

### execute the SQL statements in the file just built against the SQLite Database:

```sh
sqlite3 topo-index-scans_sqlite.db '.read file-listing.sql'
```

Now your database should have the latest listing of files.

#### check by counting the number of rows in the data table 

```sh
sqlite3 topo-index-scans_sqlite.db 'select name from pdf_files' |wc -l
```
> 463

#### and compare with the count of PDF files in the file system:

```sh
ls -l topo-index-scans/*.pdf |wc -l
```
> 463


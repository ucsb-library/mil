# Map and Imagery Library

this repository holds the various services that make up the [UCSB Map & Imagery Library][mil]

these are:

  - Air Photos Images (`ap_images`)
  - Air Photos Indexes (`ap_indexes`)
  - Air Photos Flights Catalog (`apcatalog`)
  - International Topographical Map Indexes (`mapindexes`)
  - Frame Finder (`ap_indexes/FrameFinder/`)


## Architecture

![mil.library.ucsb.edu routing/architecture diagram](/architecture.svg)

[mil]: mil.library.ucsb.edu
